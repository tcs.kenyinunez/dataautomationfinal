package com.intellect.pageobjects;

import java.io.FileWriter;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.intellect.utilities.Utilities;

public class Broker_Account_Details {
//	Maintenamineto de Banco link : id - A1.11
//	Consultar : id - A1.11.5
	//returns one int values
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Broker_Account_Details(WebDriver driver, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.id("PTY_CODE")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Codigo Contraparte",csv,textVal);
	}
	
	public int Codigo_Banco(String csv) throws Throwable {
		textVal = driver.findElement(By.id("PAY_IN_ACC")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Codigo Banco",csv,textVal);
	}
	
	public int Codigo_Sucursal(String csv) throws Throwable {
		textVal = driver.findElement(By.id("ACC_CODE1")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Codigo Sucursal",csv,textVal);
	}
	
	public int Numero_de_Cuenta_Dineraria(String csv) throws Throwable {
		textVal = driver.findElement(By.id("ACC_CODE2DESC")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Numero de Cuenta Dineraria",csv,textVal);
	}
	
	public int Codigo_Moneda(String csv) throws Throwable {
		textVal = driver.findElement(By.id("PTY_BNK_ACC")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Moneda",csv,textVal);
	}
	
	public int Codigo_Cuenta_IBAN(String csv) throws Throwable {
		textVal = driver.findElement(By.id("PTY_BNK_NAME")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta IBAN",csv,textVal);
	}
}
