package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Vault_Inward_Details_Tab {
	//return one int value
//	AdministraciÃ³n de BÃ³veda -> Ingreso -> Compra/Mercado Primario -> Consultar -> PestaÃ±a Detalles
//	AdministraciÃ³n de BÃ³veda : id - Menu_10
//	Ingreso : id - A10.1
//	Compra/Mercado Primario : id - A10.1.1
//	Consultar : id - A10.1.1.5
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Vault_Inward_Details_Tab(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Numero_Referencia_LOTE(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='LOT_REF_NO'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Numero Referencia LOTE",csv,textVal);
	}
	public int Tipo_Transaccion(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='LOT_TXN_ID'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Tipo de Transaccion",csv,textVal);
	}
	public int ISIN_del_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"ISIN del Valor",csv,textVal);
	}
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLIENT_CODE'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	public int Cantidad_Lote(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='LOT_QTY'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Cantidad de Lote",csv,textVal);
	}
	public int Precio_del_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Precio del Valor",csv,textVal);
	}
	public int Fecha_Ingreso(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='LOT_DT'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Fecha de Ingreso",csv,textVal);
	}
	public int Numero_Referencia_Operacion(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='LOT_REF_NO'])[1]")).getText().toString();	//#### pls chk the value coz its repeating	
		return Utilities.verifyField(log,writeEFile,"Numero Referencia de Operacion",csv,textVal);
	}
	public int Comentarios_Agencia(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='DE_AGENCY_CD'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Comentarios de Agencia",csv,textVal);
	}
	public int Comentarios(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='USER_COMMENTS'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Comentarios",csv,textVal);
	}
	
	}
