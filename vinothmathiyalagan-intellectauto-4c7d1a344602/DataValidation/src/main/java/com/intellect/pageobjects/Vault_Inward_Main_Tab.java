package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Vault_Inward_Main_Tab {
	//return one int value
//	AdministraciÃ³n de BÃ³veda -> Ingreso -> Compra/Mercado Primario -> Consultar -> PestaÃ±a Detalles
//	AdministraciÃ³n de BÃ³veda : id - Menu_10
//	Ingreso : id - A10.1
//	Compra/Mercado Primario : id - A10.1.1
//	Consultar : id - A10.1.1.5
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Vault_Inward_Main_Tab(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Numero_Referencia_LOTE(String csv) throws Throwable {
		driver.findElement(By.xpath("//*[@id='tabpane0']/table/tbody/tr/td[4]/a'])[1]")).click();//Pestana Detalle link tab
		textVal = driver.findElement(By.xpath("(//*[@id='LOT_REF_NO'])[2]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Numero Referencia LOTE",csv,textVal);
	}
	
	public int Cantidad_Lote_Sub(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_LOT_RNG_QTY'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Cantidad de Lote Sub",csv,textVal);
	}
	
	public int Numero_Distintivo_Desde(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_DNR_FROM'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Numero Distintivo Desde",csv,textVal);
	}
	public int Numero_Distintivo_Hasta(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_DNR_TO'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Numero Distintivo Hasta",csv,textVal);
	}
	public int Numero_Certificado_Desde(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='ALPHA_CERT_FROM'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Numero de Certificado Desde",csv,textVal);
	}
	
	public int Numero_Certificado_Hasta(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='ALPHA_CERT_TO'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Numero de Certificado Hasta",csv,textVal);
	}
	public int Total_Certificados(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Total de Certificados",csv,textVal);
	}
	public int Nro_Folio(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_FOLIO_NO'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Nro. de Folio",csv,textVal);
	}
	public int Titular1(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_HLDR1'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Titular 1",csv,textVal);
	}
	public int Titular2(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_HLDR2'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Titular 2",csv,textVal);
	}
	public int Titular3(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_HLDR3'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Titular 3",csv,textVal);
	}
	
	public int Tamano_Lote(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_LOT_SIZE'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Tamano de Lote",csv,textVal);
	}
//	public int Tamano_Lote(String csv) throws Throwable { REPEATED FIELD IN CSV - pls check
//		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_LOT_SIZE'])[1]")).getText().toString();		
//		return Utilities.verifyField(log,writeEFile,"Tamano de Lote",csv,textVal);
//	}
	public int Discrepancia(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='DISCR_CODE'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Discrepancia",csv,textVal);
	}
	public int Categoria_Lote(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_LOT_CAT'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Categoria de Lote",csv,textVal);
	}
	public int Nro_TD(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_TOT_TD'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Nro de TD",csv,textVal);
	}
	public int Otras_Discrepancias(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='DISCR_OTHERS'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Otras Discrepancias",csv,textVal);
	}
	public int Tipo_Certificado(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='VT_Inward_BuyGrid_RG_ST'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Tipo de Certificado",csv,textVal);
	}
//	Check if the below three methods are needed - validate the csv input - #####################
	public int TD_ISSUE_DATE(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='TD_ISSUE_DATE'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"TD_ISSUE_DATE",csv,textVal);
	}
	public int ALPHA_CERT_FROM(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"ALPHA_CERT_FROM",csv,textVal);
	}
	public int ALPHA_CERT_TO(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"ALPHA_CERT_TO",csv,textVal);
	}
}
