package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Client_Messaging {
	//return two int values
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Client_Messaging(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		driver.findElement(By.xpath("//*[@id='tabpane4']/table/tbody/tr/td[10]")).click();// to click Messaging button
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_CODE'])[5]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	
	public int cmValidation(String tipoMen,String nombreMen, String dir) throws Throwable {	
		String tipoMensaje="";
		String nombreMensaje="";
		String direction="";
		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='myspan_MessagingDetails']/table/tbody/tr"));	
		for(int j=0;j<rows.size();j++) {
			List<WebElement> cols = driver.findElements(By.xpath("//*[@id='myspan_MessagingDetails']/table/tbody/tr["+(j+2)+"]/td"));
//			for(int c=0;c<cols.size();c++) {
//				cols.get(c).getText()
//			}
			tipoMensaje = cols.get(1).getText();
			nombreMensaje = cols.get(0).getText();
			direction = cols.get(3).getText();	
			if(tipoMensaje.trim().equals(tipoMen.trim())) {
				break;
			}
		}
		return Utilities.verifyField(log,writeEFile,"Tipo de Mensaje",tipoMen,tipoMensaje)
				+Utilities.verifyField(log,writeEFile,"Nombre del Mensaje",nombreMen,nombreMensaje)
				+Utilities.verifyField(log,writeEFile,"Direccion",dir,direction);
	}

}
