package com.intellect.pageobjects;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class AppLogin {
	
	WebDriver driver;
	String child1;

	public AppLogin(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebDriver login(String url, String user, String pass) throws InterruptedException, AWTException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//src/main/resources/chromedriverN.exe");
		ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        driver = new ChromeDriver(options);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		String parent = driver.getWindowHandle();
		Set<String> window = driver.getWindowHandles();
		Iterator<String> it = window.iterator();		
		while(it.hasNext()) {
			String child = it.next();
			if(!parent.equals(child)) {
				driver.switchTo().window(child);
				driver.findElement(By.id("txtUserId")).sendKeys(user);
				driver.findElement(By.id("txtUserPassword")).sendKeys(pass);		
				driver.findElement(By.id("submitbtn")).click();
				Thread.sleep(2000);
//				window = driver.getWindowHandles();
//				it = window.iterator();		
//				while(it.hasNext()) {
//					child = it.next();
//					if(!parent.equals(child)) {
//						driver.switchTo().window(child);
//						int esp = 0;
//						while(esp<4) {
//							Thread.sleep(1000);
//							if(driver.getPageSource().contains("Kill Previous Session")) {
//								Robot robo = new Robot();
//								robo.keyPress(KeyEvent.VK_TAB);
//								robo.keyPress(KeyEvent.VK_ENTER);
//								Thread.sleep(3000);
//								break;
//							}else if(driver.getPageSource().contains("Bienvenido")) {
//								break;
//							}
//							esp++;
//						}
//						break;
//					}
//				}
				window = driver.getWindowHandles();
				it = window.iterator();
				while(it.hasNext()) {
					child = it.next();
					if(!parent.equals(child)) {
						driver.switchTo().window(child);
						driver.switchTo().frame(driver.findElement(By.name("mainbot")));
						Thread.sleep(3000);
						driver.findElement(By.linkText("ICUSTODY")).click();
						driver.switchTo().defaultContent();
						Thread.sleep(2000);
//						driver.close(); commented to close after execution
						break;
					}
				}
				window = driver.getWindowHandles();
				it = window.iterator();		
				while(it.hasNext()) {
					child = it.next();
					if(!parent.equals(child)) {
//						driver.switchTo().window(parent); Commented this part to close the driver at last
//						driver.close();
						Thread.sleep(1000);
						driver.switchTo().window(child);
						if(driver.getTitle().contains("ICUSTODY - Intellect")) {
							return driver;
						}
					}
				}
			}
		}
		return null;
	}
}

