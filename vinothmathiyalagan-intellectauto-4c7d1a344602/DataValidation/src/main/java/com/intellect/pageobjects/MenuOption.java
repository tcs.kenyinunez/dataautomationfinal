package com.intellect.pageobjects;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MenuOption {
	WebDriver driver;
	
	public MenuOption(WebDriver driver) {
		this.driver = driver;
	}
	
	
	public void maestro() {
//		WebElement menuFrame = driver.findElement(By.name("TopLevel"));
		
		driver.switchTo().frame("TopLevel");
		driver.findElement(By.name("Menu_1")).click();
		driver.switchTo().defaultContent();
	}
	
	public void ConsultarCliente() {
		maestro();
		driver.switchTo().frame("MenuFrame");
		driver.findElement(By.name("A1.1")).click();
		driver.findElement(By.name("A1.1.9")).click();
		driver.switchTo().defaultContent();		
	}
	
	public void search(String val) {
		driver.switchTo().frame("SearchFrame");
		driver.findElement(By.name("cboValue")).sendKeys(val);
		driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
		driver.findElement(By.name("OK_label")).click();
		driver.findElement(By.xpath("//*[@id='datagrid-row-r2-2-0']/td[1]")).click();
		driver.switchTo().defaultContent();		
	}
	
	public void menuMaestro(String module) {	
		WebElement f1 = driver.findElement(By.name("TopLevel"));
		driver.switchTo().frame(f1);
		driver.findElement(By.name("Menu_1")).click();
		driver.switchTo().defaultContent();
		WebElement f2 = driver.findElement(By.name("MenuFrame"));
		driver.switchTo().frame(f2);
		
		
		/*
		if(module.equalsIgnoreCase("Custody Account")) {
		//Custody Account
			driver.findElement(By.name("A1.1")).click();
			driver.findElement(By.name("A1.1.1")).click();
			
			
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			String parent = driver.getWindowHandle();
			Set<String> window = driver.getWindowHandles();
			Iterator<String> it = window.iterator();		
			while(it.hasNext()) {
				String child = it.next();
				if(!parent.equals(child)) {
					driver.switchTo().window(child);
					driver.findElement(By.id("CLN_NAME")).click();
					driver.findElement(By.id("CLN_NAME")).sendKeys("KENYI NUNEZ");
//					driver.findElement(By.id("txtUserPassword")).sendKeys(pass);		
//					driver.findElement(By.id("submitbtn")).click();
					Thread.sleep(2000);
			
			
				}
			}
			
		}
		*/
	
		
		//clients
		if(module.equalsIgnoreCase("Clientes")) {
			driver.findElement(By.name("A1.1")).click();
			driver.findElement(By.name("A1.1.9")).click();
		}else if(module.equalsIgnoreCase("Bank Maintenance")) {
		//Bank Maintenance
			driver.findElement(By.name("A1.11")).click();
			driver.findElement(By.name("A1.11.5")).click();
		}else if(module.equalsIgnoreCase("Client Cash Account Maintenance")) {
		//Client_CashAccount_Maintenance
			driver.findElement(By.name("A1.12")).click();
			driver.findElement(By.name("A1.12.5")).click();
		}else if(module.equalsIgnoreCase("Broker Account Details")) {
		//Broker_Account_Details
			driver.findElement(By.name("A1.13")).click();
			driver.findElement(By.name("A1.13.5")).click();
		}
		else if(module.equalsIgnoreCase("Broker Contrapartes")) {
		//Broker_Master
			driver.findElement(By.name("A1.2")).click();
			driver.findElement(By.name("A1.2.5")).click();
		}else if(module.equalsIgnoreCase("Valores")) {
		//Instrument_Master & Cronograma_Amortizacion
			driver.findElement(By.name("A1.4")).click();
			driver.findElement(By.name("A1.4.5")).click();
		}else if(module.equalsIgnoreCase("WithHolding Tax Rate Master")) {
		//WithHolding Tax Rate Master
			driver.findElement(By.name("A1.15")).click();
			driver.findElement(By.name("A1.15.5")).click();
		}else if(module.equalsIgnoreCase("Broker Depositary Mapping")) {
		//Broker Depositary Mapping
			driver.findElement(By.name("A1.3")).click();
			driver.findElement(By.name("A1.3.5")).click();		
		}else if(module.equalsIgnoreCase("Holdings")) {
		//Holdings
			driver.findElement(By.name("A2.12")).click();
			driver.findElement(By.name("A2.12.1")).click();	
		}
		
	
		driver.switchTo().defaultContent();	
	}
	
	public void menuProcessOperations(String module) {	
		WebElement f1 = driver.findElement(By.name("TopLevel"));
		driver.switchTo().frame(f1);
		driver.findElement(By.name("Menu_2")).click();
		driver.switchTo().defaultContent();
		WebElement f2 = driver.findElement(By.name("MenuFrame"));
		driver.switchTo().frame(f2);
		if(module.equalsIgnoreCase("Holdings")) {
			//Holdings
			driver.findElement(By.name("A2.12")).click();
			driver.findElement(By.name("A2.12.1")).click();
		}else if(module.equalsIgnoreCase("Term Deposit Entry")) {
		//Term Deposit Entry
			driver.findElement(By.name("A2.18")).click();
			driver.findElement(By.name("A2.18.1")).click();
			driver.findElement(By.name("A2.18.1.5")).click();
		}else if(module.equalsIgnoreCase("Transferencia Operaciones")) {
		//Transferencia de Operaciones
			driver.findElement(By.name("A2.13")).click();
			driver.findElement(By.name("A2.13.5")).click();
		}
		driver.switchTo().defaultContent();
	}
	
	public void menuAdminBoveda(String module) {	
		WebElement f1 = driver.findElement(By.name("TopLevel"));
		driver.switchTo().frame(f1);
		driver.findElement(By.name("Menu_10")).click();
		driver.switchTo().defaultContent();
		WebElement f2 = driver.findElement(By.name("MenuFrame"));
		driver.switchTo().frame(f2);
		if(module.equalsIgnoreCase("Inward")) {
			//Vault Inward Main & Vault - Inward Details
			driver.findElement(By.name("A10.1")).click();
			driver.findElement(By.name("A10.1.1")).click();
			driver.findElement(By.name("A10.1.1.5")).click();
		}else if(module.equalsIgnoreCase("Packet")) {
			
		}else {
			
		}

		driver.switchTo().defaultContent();
	}
	
	public void menuFacturacion() {	
		WebElement f1 = driver.findElement(By.name("TopLevel"));
		driver.switchTo().frame(f1);
		driver.findElement(By.name("Menu_4")).click();
		driver.switchTo().defaultContent();
		WebElement f2 = driver.findElement(By.name("MenuFrame"));
		driver.switchTo().frame(f2);
		driver.findElement(By.name("A4.2")).click();
		driver.findElement(By.name("A4.2.5")).click();
		driver.switchTo().defaultContent();
	}
	
	public WebDriver consultar(String consultarWE,String txtSearch, String testcase, String inputVal, String inputVal2, String inputVal3) throws InterruptedException, AWTException {
		WebDriverWait w = new WebDriverWait(driver,30);
		WebElement f2 = driver.findElement(By.name("MenuFrame"));
		driver.switchTo().frame(f2);
		driver.findElement(By.name(consultarWE)).click();
		driver.switchTo().defaultContent();
		WebElement f3 = driver.findElement(By.name("SearchFrame"));
		driver.switchTo().frame(f3);	
		if(!txtSearch.equals("")) {
			WebElement dropdw = driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a"));
			Select sel = new Select(dropdw);
			sel.deselectByVisibleText(txtSearch);
		}
		if(testcase.equalsIgnoreCase("Clientes")|| testcase.equalsIgnoreCase("FacturacionParametros")
		|| testcase.equalsIgnoreCase("FacturacionDependencia")|| testcase.equalsIgnoreCase("FacturacionEstructura")) {
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
			driver.findElement(By.xpath("//*[@id='OK_label']")).click();

			w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select")));
			WebElement selVal = driver.findElement(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select"));
			Select sel1 = new Select(selVal);
			sel1.selectByVisibleText("1000");	
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@id='ROWNO0']")).click();
			Thread.sleep(2000);

		}
		
		if(testcase.equalsIgnoreCase("Bank Maintenance") || testcase.equalsIgnoreCase("Broker Contrapartes") || testcase.equalsIgnoreCase("Valores") || testcase.equalsIgnoreCase("Broker Account Details")) {
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
			driver.findElement(By.xpath("//*[@id='OK_label']")).click();
			
			w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select")));
			WebElement selVal = driver.findElement(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select"));
			Select sel1 = new Select(selVal);
			sel1.selectByVisibleText("1000");	
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@id='ROWNO0']")).click();
			Thread.sleep(2000);
			
		} else if (testcase.equalsIgnoreCase("Term Deposit Entry")) {

			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")));
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")).click();
			Thread.sleep(1000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(500);
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			Thread.sleep(500);
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//*[@id='OK_label']")).click();
		
			w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select")));
			WebElement selVal = driver.findElement(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select"));
			Select sel1 = new Select(selVal);
			sel1.selectByVisibleText("1000");	
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@id='ROWNO0']")).click();
			Thread.sleep(2000);
		
		} else if (testcase.equalsIgnoreCase("Client Cash Account Maintenance")) {
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")));
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")).click();
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='_easyui_combobox_i2_2']")));
			driver.findElement(By.xpath("//*[@id='_easyui_combobox_i2_6']")).click();
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
			driver.findElement(By.xpath("//*[@id='OK_label']")).click();

			w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select")));
			WebElement selVal = driver.findElement(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select"));
			Select sel1 = new Select(selVal);
			sel1.selectByVisibleText("1000");	
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@id='ROWNO0']")).click();
			Thread.sleep(2000);
		
		} 

		else if (testcase.equalsIgnoreCase("Broker Depositary Mapping")) {
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
			driver.findElement(By.xpath("//*[@id='OK_label']")).click();
			
			w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select")));
			WebElement selVal = driver.findElement(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select"));
			Select sel1 = new Select(selVal);
			sel1.selectByVisibleText("1000");	
			Thread.sleep(1000);
		
			driver.findElement(By.xpath("//*[@id='ROWNO0']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='tabpane0']/table/tbody/tr/td[8]/a")).click();
			Thread.sleep(2000);
			
		}

		else if(testcase.equalsIgnoreCase("WithHolding Tax Rate Master")) {

			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
		
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")));
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")).click();
		 
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_DOWN);
			Thread.sleep(100);robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);
			robot.keyPress(KeyEvent.VK_ENTER);
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal2);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")));
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")).click();
			
			robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_DOWN);
			Thread.sleep(100);robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);
			robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_ENTER);
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal3);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();

			driver.findElement(By.xpath("//*[@id='OK_label']")).click();

			w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select")));
			WebElement selVal = driver.findElement(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select"));
			Select sel1 = new Select(selVal);
			sel1.selectByVisibleText("1000");	
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@id='ROWNO0']")).click();
			Thread.sleep(2000);
		}
			
		else if(testcase.equalsIgnoreCase("Holdings")) {
			
			Thread.sleep(300);w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")));
			Thread.sleep(300);driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span/span/a")).click();Thread.sleep(300);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_DOWN);Thread.sleep(100);robot.keyPress(KeyEvent.VK_ENTER);
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='cboValue']")));
			Thread.sleep(100);
			driver.findElement(By.xpath("//*[@id='cboValue']")).sendKeys(inputVal);
			driver.findElement(By.xpath("//*[@id='FindForm_New_div']/div[1]/div/div[2]/img")).click();
			Thread.sleep(1000);
			
			w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='SEARCHAll_label']"))).click();

			w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select")));
			WebElement selVal = driver.findElement(By.xpath("//*[@id='RESULTS_DIV']/div/div/div[1]/table/tbody/tr/td[1]/select"));
			Select sel1 = new Select(selVal);
			sel1.selectByVisibleText("1000");	
			Thread.sleep(1000);
		}
		return driver;
	}
}
