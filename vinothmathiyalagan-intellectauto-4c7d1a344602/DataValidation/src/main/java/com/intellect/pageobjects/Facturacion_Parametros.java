package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Facturacion_Parametros {
//	FacturaciÃ³n -> Parametros de FacturaciÃ³n Clientes -> Consultar -> PestaÃ±a Dependencia//
//	FacturaciÃ³n -> Menu_4
//	Parametros de FacturaciÃ³n Clientes -> A4.2
//	Consultar -> A4.2.5

	//return 1 int values
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Facturacion_Parametros(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Cuenta_Custodia(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='CLIENT3' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Cuenta Custodia",csv,textVal);
	}
	
	public int Moneda_Cobro(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='B_CCYCD' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Moneda de Cobro",csv,textVal);
	}
	public int Indicador_Enviar(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='SEND_TO_MASTER_IND' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Indicador Enviar a",csv,textVal);
	}
	public int Indicador_Cobrar(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='B_GEN_IND' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Indicador Cobrar a",csv,textVal);
	}
	public int Comision_Minima(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='MINIMUMFEE' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Comision Minima",csv,textVal);
	}
	public int Ajuste_ComisionFactura_Consolidada(String csv) throws Throwable {
		if(driver.findElement(By.xpath("//*[@id='IVClientBillV4']/table/tbody/tr[1]/td[2]/table/tbody/tr/td/table/tbody/tr[11]/td[1]/input")).isSelected()){
			textVal = "Y";
		}else {
			textVal="N";
		}
		return Utilities.verifyField(log,writeEFile,"Ajuste de Comision Minima en la Factura Consolidada",csv,textVal);
	}
	public int Ajuste_Comision_Tenencias(String csv) throws Throwable {
		if(driver.findElement(By.xpath("//*[@id='IVClientBillV4']/table/tbody/tr[1]/td[2]/table/tbody/tr/td/table/tbody/tr[11]/td[2]/input")).isSelected()){
			textVal = "Y";
		}else {
			textVal="N";
		}
		return Utilities.verifyField(log,writeEFile,"Ajuste de Comision Minima Solo para Tenencias",csv,textVal);
	}
	public int Indicador_Debito_Directo(String csv) throws Throwable {
		if(driver.findElement(By.xpath("//*[@id='DB_DIRECT_FLAG' and @size='1']")).isSelected()){
			textVal = "Y";
		}else {
			textVal="N";
		}
		return Utilities.verifyField(log,writeEFile,"Indicador Debito Directo",csv,textVal);
	}
	public int Cuenta_por_Cobrar(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("####")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Cuenta por Cobrar",csv,textVal);
	}
	public int Notas_Pie(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='INFORMATION' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Notas al Pie",csv,textVal);
	}
	public int Plantilla_Comisiones_Predeterminadas(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='DEFAULT_FEECLT' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Plantilla de Comisiones Predeterminadas",csv,textVal);
	}
	public int Moneda_Comision(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='FEES_CCY' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Moneda de Comision",csv,textVal);
	}
	public int Valoracion_Cartera(String csv) throws Throwable {
		if(driver.findElement(By.xpath("//*[@id='PORTFOLIO_EVAL' and @value='D']")).isSelected()){
			textVal="D";
		}else if(driver.findElement(By.xpath("//*[@id='PORTFOLIO_EVAL' and @type='radio' and @value='M']")).isSelected()) {
			textVal="M";
		}
		return Utilities.verifyField(log,writeEFile,"Valoracion de la Cartera",csv,textVal);
	}
	public int Mes_Comision_Anual(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='ANN_FEE_MONTH' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Mes de Comision Anual",csv,textVal);
	}
	
	
	public int Ajust_Comision_Maxima_Factura_Consolidada(String csv) throws Throwable {
		if(driver.findElement(By.xpath("//*[@id='IVClientBillV4']/table/tbody/tr[1]/td[2]/table/tbody/tr/td/table/tbody/tr[12]/td[1]/input")).isSelected()){
			textVal="Y";
		}else {
			textVal="N";
		}
		return Utilities.verifyField(log,writeEFile,"Ajuste de Comision Maxima en la Factura Consolidada",csv,textVal);
	}
	
	public int Ajuste_Comision_Maxima_Solo_Tenencias(String csv) throws Throwable {
		if(driver.findElement(By.xpath("//*[@id=\"IVClientBillV4\"]/table/tbody/tr[1]/td[2]/table/tbody/tr/td/table/tbody/tr[12]/td[2]/input")).isSelected()){
			textVal="Y";
		}else {
			textVal="N";
		}
		return Utilities.verifyField(log,writeEFile,"Ajuste de Comision Maxima Solo para Tenencias",csv,textVal);
	}
	
	public int Comision_Minima_Transaccion(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id=\"TXNMINFEE\" and @size='25']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Comision Minima por Transaccion",csv,textVal);
	}
	public int Comision_Maxima_Transaccion(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id=\"TXNMAXFEE\" and @size='25']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Comision Maxima por Transaccion",csv,textVal);
	}
	public int Comision_Maxima(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id=\"MAXIMUMFEE\" and @size='25']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Comision Maxima",csv,textVal);
	}
	public int Dia_GeneracionPagos_DebitoDirecto(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='DIR_DB_DATE' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Dia de Generacion de Pagos de Debito Directo",csv,textVal);
	}
	
	public int Cargos_Servicio_TransaccionBasados(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='B_BASIS' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Cargos por Servicio de Transaccion Basados en",csv,textVal);
	}
	public int Cargos_Servicio_CustodiaBasados(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='B_BASIS_CUST' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Cargos por Servicio de Custodia Basados en",csv,textVal);
	}
	public int Importe_Pendiente(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='TOTAL_OSAMOUNT' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Importe Pendiente",csv,textVal);
	}



}
