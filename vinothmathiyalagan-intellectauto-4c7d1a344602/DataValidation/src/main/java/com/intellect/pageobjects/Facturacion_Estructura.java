package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Facturacion_Estructura {
//	FacturaciÃ³n -> Parametros de FacturaciÃ³n Clientes -> Consultar -> PestaÃ±a Dependencia//
//	FacturaciÃ³n -> Menu_4
//	Parametros de FacturaciÃ³n Clientes -> A4.2
//	Consultar -> A4.2.5

	//return 2 int values
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Facturacion_Estructura(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		driver.findElement(By.xpath("//*[@id='tabpane0']/table/tbody/tr/td[6]/a")).click();// to click Pestana button
		textVal = driver.findElement(By.xpath("//*[@id='CLIENT4' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	
	public int facturacionEs(String cServ, String vDep, String  vGrupo,
			String bDel, String bAl, String com, String tCom ) throws Throwable {	
		
		String codServ="";
		String valDep="";
		String valGrupo = "";
		String baseDel = "";
		String baseAl = "";
		String commison = "";
		String tipoCom = "";

		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='myspan_GrdMTClientFee']/table/tbody/tr"));	
		for(int j=0;j<rows.size();j++) {
			List<WebElement> cols = driver.findElements(By.xpath("//*[@id='myspan_GrdMTClientFee']/table/tbody/tr["+(j+2)+"]/td"));
//			for(int c=0;c<cols.size();c++) {
//				cols.get(c).getText()
//			}
			
			codServ = cols.get(0).getText();
			valDep = cols.get(33333333).getText();	//######
			valGrupo = cols.get(8).getText();
			baseDel = cols.get(1).getText();
			baseAl = cols.get(2).getText();
			commison = cols.get(5).getText();
			tipoCom = cols.get(6).getText(); 
			if(codServ.trim().equals(cServ.trim())) { // check this condition if necessary
				break;
			}
		}
		return  Utilities.verifyField(log,writeEFile,"Codigo de Servicio",cServ,codServ)
				+Utilities.verifyField(log, writeEFile,"Valor de Dependencia",vDep,valDep)
				+Utilities.verifyField(log, writeEFile,"Valor de Grupo",vGrupo,valGrupo)
				+Utilities.verifyField(log, writeEFile,"Base de Cobro Del",bDel,baseDel)
				+Utilities.verifyField(log, writeEFile,"Base de Cobro Al",bAl,baseAl)
				+Utilities.verifyField(log, writeEFile,"Comision",com,commison)
				+Utilities.verifyField(log, writeEFile,"Tipo de Comision",tCom,tipoCom);
	}
	
	


}
