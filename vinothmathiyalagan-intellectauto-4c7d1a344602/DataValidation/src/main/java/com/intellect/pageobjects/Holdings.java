package com.intellect.pageobjects;

import java.io.FileWriter;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Holdings {
	// Procesamiento de Operaciones -> Consulta de Valores en Custodia ->
	// Valores en Custodia
	// Procesamiento de Operaciones link : //*[@id="Menu_2"]
	// Consulta de Valores en Custodia : id - A2.12
	// Valores en Custodia : id - A2.12.1
	// return one int value
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal = "";

	public Holdings(WebDriver driver, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.log = log;
		this.writeEFile = writeEFile;
	}

	public int Indicador_Propios_Terceros(String ccc, String nomCCC, String codValor, String isin, String mnemonico,
			String subTipoVal, String monedaVal, String codDep, String desDep, String dispVenta, String posContable,
			String posDisBlo, String comPenConfir, String venPenConfir, String comPenNoConfir, String venPenNoConfir,
			String recepDep, String entreDep, String tipBloque) throws Throwable {

		String vccc = "";
		String vnomCCC = "";
		String vcodValor = "";
		String visin = "";
		String vmnemonico = "";
		String vsubTipoVal = "";
		String vmonedaVal = "";
		String vcodDep = "";
		String vdesDep = "";
		String vdispVenta = "";
		String vposContable = "";
		String vposDisBlo = "";
		String vcomPenConfir = "";
		String vvenPenConfir = "";
		String vcomPenNoConfir = "";
		String vvenPenNoConfir = "";
		String vrecepDep = "";
		String ventreDep = "";
		String vtipBloque = "";

		List<WebElement> rows = driver.findElements(By.xpath("//*[contains(@id,'datagrid-row-r2-2-')]"));
		for (int j = 0; j < rows.size(); j++) {
			List<WebElement> cols = driver.findElements(By.xpath("//*[@id='datagrid-row-r2-2-" + j + "']/td"));
			for (int c = 0; c < cols.size(); c++) {
				vccc = cols.get(0).getText();
				vnomCCC = cols.get(1).getText();
				vcodValor = cols.get(2).getText();
				visin = cols.get(3).getText();
				vmnemonico = cols.get(4).getText();
				vsubTipoVal = cols.get(5).getText();
				vmonedaVal = cols.get(6).getText();
				vcodDep = cols.get(7).getText();
				vdesDep = cols.get(8).getText();
				vdispVenta = cols.get(9).getText();
				vposContable = cols.get(10).getText();
				vposDisBlo = cols.get(11).getText();
				vcomPenConfir = cols.get(12).getText();
				vvenPenConfir = cols.get(13).getText();
				vcomPenNoConfir = cols.get(14).getText();
				vvenPenNoConfir = cols.get(15).getText();
				vrecepDep = cols.get(16).getText();
				ventreDep = cols.get(17).getText();
				vtipBloque = cols.get(18).getText();
			}
		}

		return Utilities.verifyField(log, writeEFile, "Código Cuenta Custodia", ccc, vccc)
				+ Utilities.verifyField(log, writeEFile, "Nombre de Cuenta Custodia", nomCCC, vnomCCC)
				+ Utilities.verifyField(log, writeEFile, "Código Valor", codValor, vcodValor)
				+ Utilities.verifyField(log, writeEFile, "ISIN del Valor", isin, visin)
				+ Utilities.verifyField(log, writeEFile, "Mnemónico", mnemonico, vmnemonico)
				+ Utilities.verifyField(log, writeEFile, "Sub-Tipo del Valor", subTipoVal, vsubTipoVal)
				+ Utilities.verifyField(log, writeEFile, "Moneda del Valor", monedaVal, vmonedaVal)
				+ Utilities.verifyField(log, writeEFile, "Código Depositario", codDep, vcodDep)
				+ Utilities.verifyField(log, writeEFile, "Descripción del Depositario", desDep, vdesDep)
				+ Utilities.verifyField(log, writeEFile, "Disponible para la Venta", dispVenta, vdispVenta)
				+ Utilities.verifyField(log, writeEFile, "Posición Contable", posContable, vposContable)
				+ Utilities.verifyField(log, writeEFile, "Posición Disponible / Bloqueada", posDisBlo, vposDisBlo)
				+ Utilities.verifyField(log, writeEFile, "Compra Pendiente Confirmada", comPenConfir, vcomPenConfir)
				+ Utilities.verifyField(log, writeEFile, "Venta Pendiente Confirmada", venPenConfir, vvenPenConfir)
				+ Utilities.verifyField(log, writeEFile, "Compra Pendiente No Confirmada", comPenNoConfir, vcomPenNoConfir)
				+ Utilities.verifyField(log, writeEFile, "Venta Pendiente No Confirmada", venPenNoConfir, vvenPenNoConfir)
				+ Utilities.verifyField(log, writeEFile, "Recepción de Depositario", recepDep, vrecepDep)
				+ Utilities.verifyField(log, writeEFile, "Entrega de Depositario", entreDep, ventreDep)
				+ Utilities.verifyField(log, writeEFile, "Tipo de Bloque", tipBloque, vtipBloque);
	}
}
