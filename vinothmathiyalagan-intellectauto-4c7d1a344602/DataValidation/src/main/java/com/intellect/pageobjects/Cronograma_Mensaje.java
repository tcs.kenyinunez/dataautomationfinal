package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Cronograma_Mensaje {
	//return two int values
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Cronograma_Mensaje(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		driver.findElement(By.xpath("//*[@id='tabpane5']/table/tbody/tr/td[12]")).click();// to click Messaging button
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_CODE'])[6]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	
	public int crmValidation(String nom,String indFin, String diaGen,
			String indPos, String indNoPen, String mov, String decSem) throws Throwable {	
		String nombreMensaje="";
		String indFinMes="";
		String diaGeneration="";
		String indPosicion="";
		String indNoPendiente="";
		String movie="";
		String decSemanal="";
		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='myspan_MessageScheduler']/table/tbody/tr"));	
		for(int j=0;j<rows.size();j++) {
			List<WebElement> cols = driver.findElements(By.xpath("//*[@id='myspan_MessageScheduler']/table/tbody/tr["+(j+2)+"]/td"));
//			for(int c=0;c<cols.size();c++) {
//				cols.get(c).getText()
//			}
			nombreMensaje = cols.get(0).getText();
			indFinMes = cols.get(3).getText();
			diaGeneration = cols.get(7).getText();	
			indPosicion = cols.get(4).getText();
			indNoPendiente = cols.get(6).getText();
			movie = cols.get(5).getText();
			decSemanal = cols.get(8).getText();
			
			if(nombreMensaje.trim().equals(nom.trim())) {
				String temp="";
				if(diaGeneration.contains("Sun")) {temp="N";
				}else {temp="Y";}
				if(diaGeneration.contains("Mon")) {temp=temp+"N";
				}else {temp=temp+"Y";}
				if(diaGeneration.contains("Tue")) {temp=temp+"N";
				}else {temp=temp+"Y";}
				if(diaGeneration.contains("Wed")) {temp=temp+"N";
				}else {temp=temp+"Y";}
				if(diaGeneration.contains("Thu")) {temp=temp+"N";
				}else {temp=temp+"Y";}
				if(diaGeneration.contains("Fri")) {temp=temp+"N";
				}else {temp=temp+"Y";}
				if(diaGeneration.contains("Sat")) {temp=temp+"N";
				}else {temp=temp+"Y";}
				nombreMensaje = temp.trim();
				break;
			}
		}
		return Utilities.verifyField(log,writeEFile,"Nombre del Mensaje ",nom,nombreMensaje)
				+Utilities.verifyField(log,writeEFile,"Indicador Fin De Mes",indFin,indFinMes)
				+Utilities.verifyField(log,writeEFile,"Dia de Generacion",diaGen,diaGeneration)
				+Utilities.verifyField(log,writeEFile,"Indicador Posicion/Transaccion",indPos,indPosicion)
				+Utilities.verifyField(log,writeEFile,"Indicador No Pendiente ",indNoPen,indNoPendiente)
				+Utilities.verifyField(log,writeEFile,"Movimiento Cero",mov,movie)
				+Utilities.verifyField(log,writeEFile,"Declaracion Semanal",decSem,decSemanal);
	}


}
