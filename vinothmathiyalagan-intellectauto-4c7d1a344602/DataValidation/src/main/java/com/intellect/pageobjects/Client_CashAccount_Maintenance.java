package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Client_CashAccount_Maintenance {
//	Cuentas dinerarias del cliente link : id - A1.12
//	Consultar : id - A1.12.5
	//returns one int values
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
			
	public Client_CashAccount_Maintenance(WebDriver driver, Logger log, FileWriter writeEFile) {	
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		textVal = driver.findElement(By.id("CLIENT")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	
	public int Codigo_Moneda(String csv) throws Throwable {
		textVal = driver.findElement(By.id("CURRENCY_CD")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Moneda",csv,textVal);
	}
	public int Numero_Cuenta_Dineraria(String csv) throws Throwable {
		textVal = driver.findElement(By.id("GL_ACCOUNT")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Numero de Cuenta Dineraria",csv,textVal);
	}
	public int Modulo(String csv) throws Throwable {
		textVal = driver.findElement(By.id("DEFAULT_CCY")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Modulo",csv,textVal);
	}
	public int Tipo_Cuenta_Dineraria(String csv) throws Throwable {
		textVal = driver.findElement(By.id("TYPE_OF_ACC")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo de Cuenta Dineraria",csv,textVal);
	}
	public int Codigo_Banco(String csv) throws Throwable {
		textVal = driver.findElement(By.id("BANK_CD")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Banco",csv,textVal);
	}
	

}
