package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Term_Deposit_Entry {
	//return one int value
	//	Procesamiento de Operaciones -> DepÃ³sito a Plazo -> Apertura -> Consultar
	//	Procesamiento de Operaciones : id - Menu_2
	//	DepÃ³sito a Plazo : id - A2.18
	//	Apertura : id - A2.18.1
	//	Consultar : id - A2.18.1.5
	WebDriver driver;
	//int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	//public Term_Deposit_Entry(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
	public Term_Deposit_Entry(WebDriver driver, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		//this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		textVal = driver.findElement(By.name("CLIENT")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	public int Indicador_Generacion_Cargo(String csv) throws Throwable {
		if(driver.findElement(By.name("DACCOUNT_FLAG")).isSelected()) {
			textVal="Y";
		}else {
			textVal="N";
		}
		return Utilities.verifyField(log,writeEFile,"Indicador Generacion de Cargo/Abono",csv,textVal);
	}
	public int Indicador_Generacion_Mensaje_SWIFT(String csv) throws Throwable {
		if(driver.findElement(By.name("DSWIFT_FLAG")).isSelected()) {
			textVal="Y";
		}else {
			textVal="N";
		}
		return Utilities.verifyField(log,writeEFile,"Indicador Generacion del Mensaje SWIFT",csv,textVal);
		
//		textVal = driver.findElement(By.name("DSWIFT_FLAG")).getAttribute("value");	
//		return Utilities.verifyField(log,writeEFile,"Indicador Generacion del Mensaje SWIFT",csv,textVal);
	}
	public int Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.name("CRPTY_CODE")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Contraparte",csv,textVal);
	}
	public int Custodio_Global(String csv) throws Throwable {
		textVal = driver.findElement(By.name("ISSUER_CODE")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Custodio Global",csv,textVal);
	}
	public int Tipo_Deposito(String csv) throws Throwable {
		textVal = driver.findElement(By.name("####")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Tipo de Deposito",csv,textVal);
	}
	public int Codigo_Moneda(String csv) throws Throwable {
		textVal = driver.findElement(By.name("CURRENCY_CD")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Codigo Moneda",csv,textVal);
	}
	public int Fecha_Valor_Apertura(String csv) throws Throwable {
		textVal = driver.findElement(By.name("VALUE_DATE")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Fecha Valor/Fecha de Apertura",csv,textVal);
	}
	public int Numero_Dias(String csv) throws Throwable {
		textVal = driver.findElement(By.name("NO_OF_DAYS")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Numero de Dias",csv,textVal);
	}
	public int Fecha_Vencimiento(String csv) throws Throwable {
		textVal = driver.findElement(By.name("MATURITY_DATE")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Vencimiento",csv,textVal);
	}
	public int Tasa_Interes(String csv) throws Throwable {
		textVal = driver.findElement(By.name("INTEREST_RATE")).getAttribute("value");	
//		textVal=textVal.replace(",", ".");
//		textVal=textVal.replaceFirst(".", "");
		textVal = textVal.replace(",","-");
		textVal = (textVal.replace(".","")).replace("-",".");
		return Utilities.verifyField(log,writeEFile,"Tasa de Interes",csv,textVal);
	}
	public int Metodo(String csv) throws Throwable {
		textVal = driver.findElement(By.name("INT_METHOD")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Metodo",csv,textVal);
	}
	public int Compuesto(String csv) throws Throwable {
		if(driver.findElement(By.xpath("(//*[@id='DCOMPOUNDING'])[1]")).isSelected()) {//radio button
			textVal="Y";
		}else {
			textVal="N";
		}
		return Utilities.verifyField(log,writeEFile,"Compuesto",csv,textVal);
	}
	public int Frecuencia_Interes(String csv) throws Throwable {
		textVal = driver.findElement(By.name("INTEREST_FREQ")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Frecuencia de Interes",csv,textVal);
	}
	public int Comentarios(String csv) throws Throwable {
		textVal = driver.findElement(By.name("REMARK")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Comentarios",csv,textVal);
	}
	public int Codigo_ISIN(String csv) throws Throwable {
		textVal = driver.findElement(By.name("ISIN_CODE")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo ISIN",csv,textVal);
	}
	public int Importe_Apertura(String csv) throws Throwable {
		textVal = driver.findElement(By.name("DEPOSIT_AMT")).getAttribute("value");
		textVal = textVal.replace(",","-");
		textVal = (textVal.replace(".","")).replace("-",".");
		return Utilities.verifyField(log,writeEFile,"Importe de Apertura",csv,textVal);
	}
	
	public float Importe_AperturaAPP() throws Throwable {
		textVal = driver.findElement(By.name("DEPOSIT_AMT")).getAttribute("value"); 
		textVal = textVal.replace(",","-");
		textVal = (textVal.replace(".","")).replace("-",".");
		return Float.parseFloat(textVal);
	}
	
	public int Interes_Pagar(String csv) throws Throwable {
		textVal = driver.findElement(By.id("INTEREST_REC_AMT")).getAttribute("value");
//		textVal=textVal.replace(",", ".");
//		textVal=textVal.replaceFirst(".", "");
		textVal = textVal.replace(",","-");
		textVal = (textVal.replace(".","")).replace("-",".");
		return Utilities.verifyField(log,writeEFile,"Interes a Pagar",csv,textVal);
	}
	public int Importe_TotalPagar_Vencimiento(String csv) throws Throwable {
		textVal = driver.findElement(By.name("MATURITY_AMT")).getAttribute("value");
//		textVal=textVal.replace(",", ".");
//		textVal=textVal.replaceFirst(".", "");
		textVal = textVal.replace(",","-");
		textVal = (textVal.replace(".","")).replace("-",".");
		return Utilities.verifyField(log,writeEFile,"Importe Total a Pagar en el Vencimiento id",csv,textVal);
	}
	public int Fecha_Operacion(String csv) throws Throwable {
		textVal = driver.findElement(By.name("DEAL_DATE")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Fecha de Operacion",csv,textVal);
	}
	
	}
