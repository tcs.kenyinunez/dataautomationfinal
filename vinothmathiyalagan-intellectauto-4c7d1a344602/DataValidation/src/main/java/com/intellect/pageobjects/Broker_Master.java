package com.intellect.pageobjects;

import java.io.FileWriter;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.intellect.utilities.Utilities;

public class Broker_Master {
//	Counterparts link : A1.2
//	Consultar : A1.2.5
	//returns one int values
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Broker_Master(WebDriver driver, Logger log, FileWriter writeEFile) {			
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_CODE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Contraparte",csv,textVal);
	}
	
	public int Nombre_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_NAME'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre de la Contraparte",csv,textVal);
	}
	public int Nombre_corto_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_SHORTNM'])[1]")).getAttribute("value");	//PTY_NAME	
		return Utilities.verifyField(log,writeEFile,"Nombre corto de la Contraparte",csv,textVal);
	}
	public int Codigo_Contraparte_Extrangero_Depositario_DTCC(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_MAPINID'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo de Contraparte Extrangero en Depositario DTCC",csv,textVal);
	}
	public int Tipo_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_TYPE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo de Contraparte",csv,textVal);
	}
	public int Direccion1(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_ADD1'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Direccion 1",csv,textVal);
	}
	public int Direccion2(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_ADD2'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Direccion 2",csv,textVal);
	}
	public int Ciudad(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CITY'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Ciudad",csv,textVal);
	}
	public int Departamento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='STATE'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Departamento",csv,textVal);
	}
	public int Pais(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='COUNTRY'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Pais",csv,textVal);
	}
	public int Codigo_Postal(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_ADD_PIN'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Postal",csv,textVal);
	}
	public int Telefono(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_LC_TEL'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Telefono",csv,textVal);
	}
	public int Fax(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_FAX'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fax",csv,textVal);
	}
	public int Correo_Electronico(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Correo Electronico",csv,textVal);
	}
	public int Nombre_Persona_Contacto01(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_CONTACT1'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre de Persona de Contacto 01",csv,textVal);
	}
	public int Nombre_Persona_Contacto02(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_CONTACT2'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre de Persona de Contacto 02",csv,textVal);
	}
	public int Nombre_Swift(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_SWF_NAME'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre del Swift",csv,textVal);
	}
	public int Direccion_del_Swift(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_SWF_ADD'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Direccion del Swift",csv,textVal);
	}
	public int Nro_Documento_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_PAN'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Nro de Documento de la Contraparte",csv,textVal);
	}
	public int Nombre_Local_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_LC_NAME'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre Local de la Contraparte",csv,textVal);
	}
	public int Direccion_Local_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_LC_ADD'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Direccion Local de la Contraparte",csv,textVal);
	}
	public int Nro_Telefono_Local_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_LC_TEL'])[1]")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Nro. Telefono Local de la Contraparte",csv,textVal);
	}
	public int Correo_Electronico_Contraparte_Local(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_EMAIL'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Correo Electronico de la Contraparte Local",csv,textVal);
	}
	public int Nro_Fax_Local_Contraparte(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_LC_FAX'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Nro. Fax Local de la Contraparte",csv,textVal);
	}
	public int Codigo_BIC(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_BIC_CODE'])[1]")).getAttribute("value"); 		
		return Utilities.verifyField(log,writeEFile,"Codigo BIC",csv,textVal);
	}
	public int Tipo_Documento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_DOC_TYPE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo de Documento",csv,textVal);
	}

	
}
