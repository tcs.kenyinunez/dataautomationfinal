package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Broker_Depository_Mapping {
	//return one int value
//	Counterparts link : A1.2
//	Consultar : A1.2.5
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
		
	public Broker_Depository_Mapping(WebDriver driver, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Contraparte(String csv) throws Throwable {
		driver.findElement(By.xpath("//*[@id='tabpane3']/table/tbody/tr/td[8]")).click();// to click Depositorio button
		textVal = driver.findElement(By.xpath("(//*[@id='PTY_CODE1'])[3]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Contraparte",csv,textVal);
	}
	
	
	/*
	public int Codigo_Depositario(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTYDEPO_DEPO_CODE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Depositario",csv,textVal);
	}
	
	public int ID_Depositario(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTYDEPO_PTY_DEPO_MAP_DP_ID'])[1] ")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"ID de Depositario",csv,textVal);
	}
	public int Codigo_Contraparte_Depositario(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTYDEPO_PTY_DEPO_MAP_CLIENT_ID'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo de Contraparte en el Depositario",csv,textVal);
	}
	public int Codigo_Bolsa(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='PTYDEPO_EXCH_CODE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Bolsa",csv,textVal);
	}
	*/
	
	
	public int cmValidation(String codDepositorio,String idDepositario, String codContraparteDepositario , String codBolsa) throws Throwable {	
		
		String ccodigoDepositario="";
		String ciDdeDepositario="";
		String ccodigodeContraparteenelDepositario="";
		String ccodigoBolsa="";
		
//		String tipoMensaje="";
//		String nombreMensaje="";
//		String direction="";
		
															  
		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='myspan_PTYDEPO']/table/tbody/tr"));	
		for(int j=0;j<rows.size();j++) {
			List<WebElement> cols = driver.findElements(By.xpath("//*[@id='myspan_PTYDEPO']/table/tbody/tr["+(j+2)+"]/td"));
//			for(int c=0;c<cols.size();c++) {
//				cols.get(c).getText()
//			}
			
			ccodigoDepositario = cols.get(0).getText();
			ciDdeDepositario = cols.get(2).getText();
			ccodigodeContraparteenelDepositario = cols.get(1).getText();
			ccodigoBolsa = cols.get(3).getText();
			
//			tipoMensaje = cols.get(1).getText();
//			nombreMensaje = cols.get(0).getText();
//			direction = cols.get(3).getText();	
			
			
			if(ccodigoDepositario.trim().equals(codDepositorio.trim())) {
				break;
			}
		}
//		return Utilities.verifyField(log,writeEFile,"Tipo de Mensaje",tipoMen,tipoMensaje)
//				+Utilities.verifyField(log,writeEFile,"Nombre del Mensaje",nombreMen,nombreMensaje)
//				+Utilities.verifyField(log,writeEFile,"Direccion",dir,direction);
		
		
		return Utilities.verifyField(log,writeEFile,"Codigo Depositario",codDepositorio,ccodigoDepositario)
				+Utilities.verifyField(log,writeEFile,"ID de Depositario",idDepositario,ciDdeDepositario)
				+Utilities.verifyField(log,writeEFile,"Codigo de Contraparte en el Depositario",codContraparteDepositario,ccodigodeContraparteenelDepositario)
				+Utilities.verifyField(log,writeEFile,"Codigo Bolsa",codBolsa,ccodigoBolsa);
	}


}
