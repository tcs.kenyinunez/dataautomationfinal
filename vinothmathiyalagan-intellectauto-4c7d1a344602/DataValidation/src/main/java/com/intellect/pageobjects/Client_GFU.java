package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Client_GFU {
	
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Client_GFU(WebDriver driver, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='CLN_CODE']")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	
	public int Categoria_del_Cliente(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_TYPE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Categoria del Cliente",csv,textVal);
	}
	
	public int Cuenta_Maestra(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_MASTER'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Cuenta Maestra",csv,textVal);
	}
	
	public int Nombre_de_Cuenta_Custodia(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_NAME'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre de Cuenta Custodia",csv,textVal);
	}
	
	public int Nombre_del_Cliente(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_SHORTNM'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre del Cliente",csv,textVal);
	}
	
	public int Fecha_de_Ingreso(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Fecha de Ingreso",csv,"");
	}
	public int Registrado_por(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Registrado por",csv,"");
	}
	public int Fecha_de_Registro(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Registro",csv,"");
	}
	public int Registro_de_Accesos(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Registro de Accesos",csv,"");
	}
	public int Informacion_Adicional_de_Direccion(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Informacion Adicional de Direccion",csv,"");
	}
	public int Validado_por(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Validado por",csv,"");
	}
	public int Fecha_de_Validacion(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Validacion",csv,"");
	}
	public int Direccion1(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_ADD_1'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Direccion 1",csv,textVal);
	}
	public int Direccion2(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_ADD_2'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Direccion 2",csv,textVal);
	}
	public int Telefono(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_TEL'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Telefono",csv,textVal);
	}
	public int FAX(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_FAX'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"FAX",csv,textVal);
	}
	public int Persona_de_Contacto1(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_CONTACT1'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Persona de Contacto 1",csv,textVal);
	}
	public int Persona_de_Contacto2(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_CONTACT2'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Persona de Contacto 2",csv,textVal);
	}
	public int Numero_de_Documento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_PAN'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Numero de Documento",csv,textVal);
	}
	
	public int Poliza_Broker_Requerido(String csv) throws Throwable {
		boolean chk = driver.findElement(By.xpath("(//*[@id='CLN_BCN_REQD'])[1]")).isSelected();	
		if(chk) {
			textVal = "Y";
		}else {
			textVal = "N";
		}
		return Utilities.verifyField(log,writeEFile,"Poliza Broker Requerido",csv,textVal);
	}
	public int Indicador_Cuenta_Maestra_Sub_Cuenta(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_PAN'])[1]")).getAttribute("value");
		int intVal = Integer.parseInt(textVal);
		intVal = intVal*1;
		return Utilities.verifyField(log,writeEFile,"Indicador Cuenta Maestra y Sub Cuenta",csv,String.valueOf(intVal));
	}
	public int Pais(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='COUNTRY'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Pais",csv,textVal);
	}
	public int Estado_Departamento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='STATE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Estado/Departamento",csv,textVal);
	}
	public int Ciudad(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CITY'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Ciudad",csv,textVal);
	}
	public int Tipo_de_Documento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_DOC_TYPE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo de Documento",csv,textVal);
	}
	public int Tipo_de_Persona(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_PERSON_TYPE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo de Persona",csv,textVal);
	}
	public int Facturable(String csv) throws Throwable {
		if(driver.findElement(By.xpath("(//*[@id='CLN_BILLABLEIND'])[1]")).isSelected()) {
			textVal="Y";
		}else {
			textVal="N";
		}		
		return Utilities.verifyField(log,writeEFile,"Facturable",csv,textVal);
	}
	public int Pais_del_Documento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_COUNTRY'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Pais del Documento",csv,textVal);
	}
	public int De_informe_cliente_Preferencias_idioma(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_REP_PREF_LANG'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"De informe de cliente Preferencias de idioma",csv,textVal);
	}
	
	public int Domicilio(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Domicilio",csv,"");
	}
	public int Codigo_Moneda(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_CURRENCY'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Moneda",csv,textVal);
	}
	public int La_facturacion_cliente_Preferencias_Idioma(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_BILL_PREF_LANG'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"La facturacion del cliente Preferencias de Idioma",csv,textVal);
	}
	public int Codigo_CIC(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_CUST_IDENT'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo CIC",csv,textVal);
	}
	public int Indicador_Cuenta_Mancomunada_Individual(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='JOINT_ACC'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Indicador Cuenta Mancomunada/Individual",csv,textVal);
	}
	public int Fecha_Nacimiento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_DOB'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Nacimiento",csv,textVal);
	}
	public int Estatus(String csv) throws Throwable {
//		textVal = driver.findElement(By.xpath("(//*[@id='#####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Estatus",csv,textVal);
	}
	public int Codigo_Postal(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_ADD_PIN'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Postal",csv,textVal);
	}
	public int Fecha_Registro_Cliente(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='ACC_OPN_DT'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Registro Cliente",csv,textVal);
	}
	public int Indicador_Propios_Terceros(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_OUR_THEIR_IND'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Indicador Propios/Terceros",csv,textVal);
	}
	
}
