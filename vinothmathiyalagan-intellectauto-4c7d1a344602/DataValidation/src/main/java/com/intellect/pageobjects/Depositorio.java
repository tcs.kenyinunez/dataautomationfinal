package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Depositorio {
	//return two int values
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Depositorio(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		driver.findElement(By.xpath("//*[@id='tabpane0']/table/tbody/tr/td[8]")).click();// to click Deposition button
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_CODE'])[4]")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	
	public int DepoValidation(String dCode,String cCode, String id) throws Throwable {	
		String codDepo="";
		String codCliente="";
		String idDepo="";
		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='myspan_Depository']/table/tbody/tr"));	
		for(int j=0;j<rows.size();j++) {
			List<WebElement> cols = driver.findElements(By.xpath("//*[@id='myspan_Depository']/table/tbody/tr["+(j+2)+"]/td"));
//			for(int c=0;c<cols.size();c++) {
//				cols.get(c).getText()
//			}
			codDepo = cols.get(0).getText();
			codCliente = cols.get(2).getText();
			idDepo = cols.get(1).getText();	
			if(idDepo.trim().equals(id.trim())) {
				break;
			}
		}
		return Utilities.verifyField(log,writeEFile,"Tipo de Mensaje",dCode,codDepo)
				+Utilities.verifyField(log,writeEFile,"Nombre del Mensaje",cCode,codCliente)
				+Utilities.verifyField(log, writeEFile,"Direccion",id,idDepo);
	}
	


}
