package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Transferencia_Posiciones {
	//return one int values
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Transferencia_Posiciones(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Fecha_Transaccion(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='DEAL_DATE']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Fecha de Transaccion",csv,textVal);
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='CLIENT']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	public int Fecha_Desbloqueo(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='UNBLOCK_DATE']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Fecha de Desbloqueo",csv,textVal);
	}
	public int TipoBloqueo(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='BLK_REASON']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Tipo de Bloqueo",csv,textVal);
	}
	public int Depositario(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("//*[@id='TO_LOCATION']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Depositario",csv,textVal);
	}	


}
