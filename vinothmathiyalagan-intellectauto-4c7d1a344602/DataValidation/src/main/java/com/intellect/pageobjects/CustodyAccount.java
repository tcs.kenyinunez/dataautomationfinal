package com.intellect.pageobjects;

import java.io.FileWriter;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.intellect.utilities.Utilities;

public class CustodyAccount {
//	Maintenamineto de Banco link : id - A1.11
//	Consultar : id - A1.11.5
	//returns one int values
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public CustodyAccount(WebDriver driver, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	

	
	public int Codigo_Banco(String csv) throws Throwable {
		textVal = driver.findElement(By.id("BANK_CD")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Banco",csv,textVal);
	}
	
	public int Codigo_Sucursal(String csv) throws Throwable {
		textVal = driver.findElement(By.id("BRANCH_CD")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Codigo Sucursal",csv,textVal);
	}
	public int Nombre_del_Banco(String csv) throws Throwable {
		textVal = driver.findElement(By.id("BANK_CDDESC1")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Nombre del Banco",csv,textVal);
	}
	public int Nombre_Sucursal(String csv) throws Throwable {
		textVal = driver.findElement(By.id("BRANCH_NAME")).getAttribute("value");
		return Utilities.verifyField(log,writeEFile,"Nombre de Sucursal",csv,textVal);
	}
	public int Codigo_SWIFT(String csv) throws Throwable {
		textVal = driver.findElement(By.id("SWIFT_CODE")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Codigo SWIFT",csv,textVal);
	}
	public int Direccion_Sucursal1(String csv) throws Throwable {
		textVal = driver.findElement(By.id("BRANCH_ADDRESS1")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Direccion de Sucursal 1",csv,textVal);
	}
	public int Direccion_Sucursal2(String csv) throws Throwable {
		textVal = driver.findElement(By.id("BRANCH_ADDRESS2")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Direccion de Sucursal 2",csv,textVal);
	}
	public int Pais(String csv) throws Throwable {
		textVal = driver.findElement(By.id("COUNTRY")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Pais",csv,textVal);
	}
	public int Codigo_Postal(String csv) throws Throwable {
		textVal = driver.findElement(By.id("PIN_CODE")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Postal",csv,textVal);
	}
	public int Persona_Contacto(String csv) throws Throwable {
		textVal = driver.findElement(By.id("CONTACT_PERSON")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Persona de Contacto",csv,textVal);
	}
	public int Numero_Telefono_PersonaContacto(String csv) throws Throwable {
		textVal = driver.findElement(By.id("CONTACT_PERSON_PHONE_NUMBER")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Numero de Telefono de Persona de Contacto",csv,textVal);
	}
	
	public int Trans_Chip_No(String csv) throws Throwable {
		textVal = driver.findElement(By.id("TRANS_CHIP_NO")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Trans Chip No",csv,textVal);
	}
	
	public int Indicador_Banco_Intermediario(String csv) throws Throwable {
		if(driver.findElement(By.id("CORRESPONDENT_BNK")).isSelected()) {
			textVal = "Y";
		}else { textVal = "N";}
		return Utilities.verifyField(log,writeEFile,"Indicador Banco Intermediario",csv,textVal);
	}
	public int Codigo_Banco_ntermediario(String csv) throws Throwable {
		textVal = driver.findElement(By.id("#####")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Banco Intermediario",csv,textVal);
	}
}
