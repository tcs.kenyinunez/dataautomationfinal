package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Cronograma_Amortizacion {
//	Valores link :id - A1.4
//	Consultar :  id - A1.4.5
	//return one int value
	WebDriver driver;
	//int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	//public Cronograma_Amortizacion(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
	public Cronograma_Amortizacion(WebDriver driver, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		//this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	
	public int tableValidation(String fecha,String percent, String valor) throws Throwable {	
		String fechaAmor="";
		String perAmor="";
		String valAmor="";
		driver.findElement(By.xpath("//*[@id='tabpane0']/table/tbody/tr/td[8]/a")).click(); // to click the button tab
		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='myspan_REDEMPTIONTAB']/table/tbody/tr"));	
		for(int j=0;j<rows.size();j++) {
			List<WebElement> cols = driver.findElements(By.xpath("//*[@id='myspan_REDEMPTIONTAB']/table/tbody/tr["+(j+2)+"]/td"));
//			for(int c=0;c<cols.size();c++) {
//				cols.get(c).getText()
//			}
//			fechaAmor = cols.get(0).getText();
//			perAmor = cols.get(1).getText();
//			valAmor = cols.get(2).getText();
			
			fechaAmor = cols.get(0).getAttribute("value");
			perAmor = cols.get(1).getAttribute("value");
			valAmor = cols.get(2).getAttribute("value");
			if(valAmor.trim().equals(valor.trim())) { //Check if this field comparison is OK
				break;
			}
		}
		return Utilities.verifyField(log,writeEFile,"Fecha de Amortizacion",fecha,fechaAmor)
				+Utilities.verifyField(log,writeEFile,"Porcentaje de Amortizacion",percent,perAmor)
				+Utilities.verifyField(log, writeEFile,"Valor de Amortizacion",valor,valAmor);
	}
	
	


}
