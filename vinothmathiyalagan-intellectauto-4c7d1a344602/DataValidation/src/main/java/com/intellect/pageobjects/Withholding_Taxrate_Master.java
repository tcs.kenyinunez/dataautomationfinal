package com.intellect.pageobjects;

import java.io.FileWriter;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.intellect.utilities.Utilities;

public class Withholding_Taxrate_Master {

	//returns one int value
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	 
	public Withholding_Taxrate_Master(WebDriver driver, Logger log, FileWriter writeEFile) {	
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Pais_Ingreso(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='NATION_CD'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Pais de Ingreso",csv,textVal);
	}
	
	public int Tipo_Evento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CA_EVENT'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo de Evento",csv,textVal);
	}
	public int Estatus_Residencial_Cliente(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_DOMICILE'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Estatus Residencial del Cliente",csv,textVal);
	}
	public int Tipo_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_TYPE'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo de Valor",csv,textVal);
	}
	public int Subtipo_Valor (String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_SUB_TYPE'])[2]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Subtipo de Valor ",csv,textVal);
	}
	public int Fecha_Vigencia(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='EFFECTIVE_DT'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Vigencia",csv,textVal);
	}
	public int Tasa_Impositiva(String csv) throws Throwable {
	    
		textVal = driver.findElement(By.xpath("//*[@id='CUSTODYTAXRT']")).getAttribute("value");
		
		if(isNumeric(textVal)==true){ 
			return Utilities.verifyField(log,writeEFile,"Tasa Impositiva",csv,textVal);
		}else{
			textVal = textVal.replace(",", ".");
			textVal = textVal.replaceFirst(".", "");
			return Utilities.verifyField(log,writeEFile,"Tasa Impositiva",csv,textVal);
		}
		
	}
	
	public static boolean isNumeric(String textVal){
		boolean resultado;
		try {
			Integer.parseInt(textVal);
			resultado = true;
		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}
	
	public int Monto_Minimo_Impuestos(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='MIN_TAX_AMT'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Monto Minimo de Impuestos",csv,textVal);
	}
	public int Monto_Maxmimo_Impuestos(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='MAX_TAX_AMT'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Monto Maximo de Impuestos",csv,textVal);
	}
	public int Tratado(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tratado",csv,textVal);
	}
	public int Tipo_Persona(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CLN_PERSON_TYPE'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Tipo de Persona",csv,textVal);
	}
}
