package com.intellect.pageobjects;

import java.io.FileWriter;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.intellect.utilities.Utilities;

public class Instrument_Master {
//	Valores link :id - A1.4
//	Consultar :  id - A1.4.5
	//return one int values
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
		
	public Instrument_Master(WebDriver driver, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_CODE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Valor",csv,textVal);
	}
	
	public int Codigo_Ubicacion(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Ubicacion",csv,textVal);
	}
	public int Codigo_moneda_valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CURRENCY_CD'])[2]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo moneda del valor",csv,textVal);
	}
	public int Valor_Nominal_Actual(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='NOMINAL_VALUE'])[1]")).getAttribute("value");		
		textVal = textVal.replace(",","-");
		textVal = (textVal.replace(".","")).replace("-",".");
		return Utilities.verifyField(log,writeEFile,"Valor Nominal Actual",csv,textVal);
	}
	public float Valor_Nominal_ActualAPP() throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='NOMINAL_VALUE'])[1]")).getAttribute("value");		
		textVal = textVal.replace(",","-");
		textVal = (textVal.replace(".","")).replace("-",".");
		return Float.parseFloat(textVal);
	}
	
	public int Tipo_del_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo del Valor",csv,textVal);
	}
	public int Subtipo_del_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_SUB_TYPE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Subtipo del Valor",csv,textVal);
	}
	public int Negociable(String csv) throws Throwable {
		if(driver.findElement(By.xpath("(//*[@id='TRADABLE'])[1]")).isSelected()) {
			textVal = "Y";
		}else {
			textVal = "N";
		}
		return Utilities.verifyField(log,writeEFile,"Negociable",csv,textVal);
	}
	public int Cotizado(String csv) throws Throwable {
		if(driver.findElement(By.xpath("(//*[@id='LISTED_IND'])[1]")).isSelected()) {
			textVal = "Y";
		}else {
			textVal = "N";
		}
		return Utilities.verifyField(log,writeEFile,"Cotizado",csv,textVal);
	}
	public int Fecha_Ingreso(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Fecha de Ingreso",csv,textVal);
	}
	public int Mnemonico(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_NAME'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Mnemonico",csv,textVal);
	}
	public int Nombre_corto_del_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_SHORTNM'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre corto del Valor",csv,textVal);
	}
	public int Codigo_ISIN_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_ISIN'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo ISIN del Valor",csv,textVal);
	}
	public int Codigo_Valor_Principal(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_PARENT'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo de Valor Principal",csv,textVal);
	}
	public int Fecha_Inicio_Emision(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='ISSUE_DATE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Inicio de Emision",csv,textVal);
	}
	public int Fecha_Vencimiento_Emision(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='MATURE_DT'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Vencimiento de Emision",csv,textVal);
	}
	public int Naturaleza_Interes(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INT_TYP'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Naturaleza del Interes",csv,textVal);
	}
	public int Tasa_Cupon_Interes(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_INT_RATE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tasa Cupon de Interes",csv,textVal);
	}
	public int Frecuencia_interes(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='FREQ_CODE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Frecuencia del interes",csv,textVal);
	}
	public int Tipo_Calculo_Interes(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INT_ACCTYP'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Tipo de Calculo de Interes",csv,textVal);
	}
	public int Fecha_Interes_Anterior(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='LAST_PYMT_DATE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Interes Anterior",csv,textVal);
	}
	public int Siguiente_Fecha_Interes(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='NEXT_PAY_DATE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Siguiente Fecha de Interes",csv,textVal);
	}
	public int Lote_Comercializable(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='MIN_FMT_DL_QTY'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Lote Comerciable",csv,textVal);
	}
	public int Codigo_Emisor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CMP_CODE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo Emisor",csv,textVal);
	}
	public int Codigo_del_Registrante(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Codigo del Registrante",csv,textVal);
	}
	public int Nombre_SWIFT(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_SWF_NAME'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre SWIFT",csv,textVal);
	}
	public int Colocado_Privadamente(String csv) throws Throwable {
		if(driver.findElement(By.xpath("(//*[@id='INSTR_PRIV_PLACED'])[1]")).isSelected()){ //check the id value once ####
			textVal = "Y";
		}else {
			textVal = "N";
		}
		return Utilities.verifyField(log,writeEFile,"Colocado Privadamente",csv,textVal);
	}
	public int Asegurado(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Asegurado",csv,textVal);
	}
	public int Fecha_Aseguramiento(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Aseguramiento",csv,textVal);
	}
	public int Indicador_PutCall(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Indicador Put/Call",csv,textVal);
	}
	public int Amortizacion_Parcial(String csv) throws Throwable {
		if(driver.findElement(By.xpath("(//*[@id='INSTR_PART_RD_FLG'])[1]")).isSelected()){ //check the id value once ####
			textVal = "Y";
		}else {
			textVal = "N";
		}
		return Utilities.verifyField(log,writeEFile,"Amortizacion Parcial",csv,textVal);
	}
	public int Fecha_cambio_LOA(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_EXDT_LOA'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de cambio de LOA",csv,textVal);
	}
	public int Primera_Fecha_Interes(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_FST_PYMT_DT'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Primera Fecha de Interes",csv,textVal);
	}
	public int Tasa_Dividendo(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_DIV_RATE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tasa de Dividendo",csv,textVal);
	}
	public int Desmaterializado(String csv) throws Throwable {
		if(driver.findElement(By.xpath("(//*[@id='INSTR_IS_DEMAT'])[1]")).isSelected()){ //check the id value once ####
			textVal = "Y";
		}else {
			textVal = "N";
		}
		return Utilities.verifyField(log,writeEFile,"Desmaterializado",csv,textVal);
	}
	public int Notas_Observaciones(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_REMARK'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Notas - Observaciones",csv,textVal);
	}
	public int Registrado_por(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Registrado por",csv,textVal);
	}
	public int Fecha_Registro(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Registro",csv,textVal);
	}
	public int Validado_por(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Validado por",csv,textVal);
	}
	public int Fecha_Validacion(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Fecha de Validacion",csv,textVal);
	}
	public int Registro_Accesos(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Registro de Accesos",csv,textVal);
	}
	public int Indicador_Renta_Fija(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='####'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Indicador Renta Fija",csv,textVal);
	}
	
	public int Valor_Nominal_Inicial(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='INSTR_TOT_NOM_VAL'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Valor Nominal Inicial",csv,textVal);
	}
	public int Pais_Cotizacion_Bolsa(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='COUNTRY'])[1]")).getAttribute("value");	
		return Utilities.verifyField(log,writeEFile,"Pais de Cotizacion en Bolsa",csv,textVal);
	}
	public int Tipo_Cantidad(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='UDF_TEXT_2'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo de Cantidad",csv,textVal);
	}
	public int Extension_Decimal_para_Valor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='DEC_LEN_MF'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Extension Decimal para el Valor",csv,textVal);
	}
	
	public int Tipo_Dividendo(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='DIVIDEND_TYPE'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Tipo Dividendo",csv,textVal);
	}

	public int Nombre_del_Emisor(String csv) throws Throwable {
		textVal = driver.findElement(By.xpath("(//*[@id='CMP_CODEDESC'])[1]")).getAttribute("value");		
		return Utilities.verifyField(log,writeEFile,"Nombre del Emisor",csv,textVal);
	}
}
