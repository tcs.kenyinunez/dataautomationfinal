package com.intellect.pageobjects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.intellect.utilities.Utilities;

public class Facturacion_Dependencia {
//	FacturaciÃ³n -> Parametros de FacturaciÃ³n Clientes -> Consultar -> PestaÃ±a Dependencia//
//	FacturaciÃ³n -> Menu_4
//	Parametros de FacturaciÃ³n Clientes -> A4.2
//	Consultar -> A4.2.5

	//return 3 int values
	WebDriver driver;
	int value;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	public Facturacion_Dependencia(WebDriver driver, int value, Logger log, FileWriter writeEFile) {
		this.driver = driver;
		this.value=value;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	public int Codigo_Cuenta_Custodia(String csv) throws Throwable {
		driver.findElement(By.xpath("//*[@id='tabpane0']/table/tbody/tr/td[4]/a")).click();// to click Pestana button
		textVal = driver.findElement(By.xpath("//*[@id='CLIENT5' and @class='input']")).getText().toString();		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	
	public int cobro(String csv) throws Throwable {
		if(driver.findElement(By.xpath("//*[@id='GrdCln_CHARGE_BASIS' and @value='T']")).isSelected()) {
			textVal="T";
		}else if(driver.findElement(By.xpath("//*[@id='GrdCln_CHARGE_BASIS' and @value='S']")).isSelected()) {
			textVal = "S";
		}		
		return Utilities.verifyField(log,writeEFile,"Codigo Cuenta Custodia",csv,textVal);
	}
	
	public int facturacion(String cServ, String cDep, String  comMin,
			String comNul, String comMax, String baseCom, String com ) throws Throwable {	
		
		String codServ="";
		String codDep="";
		String comMinimo = "";
		String comNull = "";
		String comMaximo = "";
		String baseCommison = "";
		String commison = "";
		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='myspan_GrdCln']/table/tbody/tr"));	
		for(int j=0;j<rows.size();j++) {
			List<WebElement> cols = driver.findElements(By.xpath("//*[@id='myspan_GrdCln']/table/tbody/tr["+(j+2)+"]/td"));
//			for(int c=0;c<cols.size();c++) {
//				cols.get(c).getText()
//			}
			
			codServ = cols.get(0).getText();
			codDep = cols.get(33333333).getText();	//######
			comMinimo = cols.get(4).getText();
			comNull = cols.get(5).getText();
			comMaximo = cols.get(6).getText();
			baseCommison = cols.get(8).getText();
			commison = cols.get(4444444).getText(); //######
			if(codServ.trim().equals(cServ.trim())) { // check this condition if necessary
				break;
			}
		}
		return  Utilities.verifyField(log,writeEFile,"Codigo de Servicio",cServ,codServ)
				+Utilities.verifyField(log, writeEFile,"Codigo de Dependencia",cDep,codDep)
				+Utilities.verifyField(log, writeEFile,"Comision Minima",comMin,comMinimo)
				+Utilities.verifyField(log, writeEFile,"Comision Nula",comNul,comNull)
				+Utilities.verifyField(log, writeEFile,"Comision Maxima",comMax,comMaximo)
				+Utilities.verifyField(log, writeEFile,"Base de Comision + Base de Cobro",baseCom,baseCommison)
				+Utilities.verifyField(log, writeEFile,"Comision",com,commison);
	}
	
	


}
