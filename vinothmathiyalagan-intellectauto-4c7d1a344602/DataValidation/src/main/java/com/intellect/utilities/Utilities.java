package com.intellect.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


//import com.intellect.testcases.TC01_Cliente;

public class Utilities {
	
	public static String takeSnapShot(WebDriver driver) throws Exception{

        TakesScreenshot scrShot =((TakesScreenshot)driver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        String screen = System.getProperty("user.dir")+"/Reports/Report/Docs/file_"+Utilities.dateTime(0)+".png";
        File DestFile=new File(screen);
        FileUtils.copyFile(SrcFile, DestFile);
        return screen;
    }
	
	public static void reportHeader(FileWriter fw, String scenario, String script_name) throws Throwable {
		
		String bcpImg = System.getProperty("user.dir")+"/src/main/resources/bcp.png";
		String tcsImg = System.getProperty("user.dir")+"/src/main/resources/tcs.png";
		
		fw.write("<meta http-equiv='Content-TYP' content='text/html; charset=utf-8' />\n");
		fw.write("<html>\n");
		fw.write("<title>EVIDENCIA DE PRUEBA AUTOMATIZADA</title>\n");
		fw.write("<body><CENTER>\n");
		fw.write("<!-- CSS goes in the document HEAD or added to your external stylesheet -->\n");
		fw.write("<style TYP='text/css'>\n");
		fw.write("table.formatoTexto {\n");
		fw.write("font-family: verdana,arial,sans-serif;\n");
		fw.write("font-size:11px;\n");
		fw.write("color:#333333;\n");
		fw.write("border-width: 1px;\n");
		fw.write("border-color: #a9c6c9;\n");
		fw.write("border-collapse: collapse;\n");
		fw.write("}\n");
		fw.write("table.altrowstable {\n");
		fw.write("font-family: verdana,arial,sans-serif;\n");
		fw.write("font-size:11px;\n");
		fw.write("color:#333333;\n");
		fw.write("border-width: 1px;\n");
		fw.write("border-color: #a9c6c9;\n");
		fw.write("border-collapse: collapse;\n");
		fw.write("}\n");
		fw.write("table.altrowstable th {\n");
		fw.write("border-width: 1px;\n");
		fw.write("padding: 8px;\n");
		fw.write("border-style: solid;\n");
		fw.write("border-color: #a9c6c9;\n");
		fw.write("}\n");
		fw.write("table.altrowstable td {\n");
		fw.write("border-width: 1px;\n");
		fw.write("padding: 8px;\n");
		fw.write("border-style: solid;\n");
		fw.write("border-color: #a9c6c9;\n");
		fw.write("}\n");
		fw.write(".oddrowcolor{\n");
		fw.write("background-color:#d4e3e5;\n");
		fw.write("}\n");
		fw.write(".evenrowcolor{\n");
		fw.write("background-color:#c3dde0;\n");
		fw.write("}\n");
		fw.write("</style>\n");
		fw.write("<script language='JavaScript1.2'>\n");
		fw.write("var zoomfactor=0.5\n");
		fw.write("function zoomhelper(){\n");
		fw.write("whatcache.style.width=1300\n");
		fw.write("whatcache.style.height=1100\n");
		fw.write("}\n");
		fw.write("function zoom(originalW, originalH, what, state)\n");
		fw.write("{\n");
		fw.write("if (!document.all&&document.getElementById)\n");
		fw.write("return\n");
		fw.write("whatcache=eval('document.images.'+what)\n");
		fw.write("prefix=(state=='in')? 1 : -1\n");
		fw.write("if (whatcache.style.width==''||state=='restore')\n");
		fw.write("{\n");
		fw.write("whatcache.style.width=originalW + 111\n");
		fw.write("whatcache.style.height=originalH + 100\n");
		fw.write("if (state=='restore')\n");
		fw.write("return\n");
		fw.write("}\n");
		fw.write("else\n");
		fw.write("{\n");
		fw.write("zoomhelper()\n");
		fw.write("}\n");
		fw.write("beginzoom=setInterval('zoomhelper()',10)\n");
		fw.write("}\n");
		fw.write("function clearzoom(){\n");
		fw.write("if (window.beginzoom)\n");
		fw.write("clearInterval(beginzoom)\n");
		fw.write("}\n");
		fw.write("</script>\n");
		fw.write("<script TYP='text/javascript'>\n");
		fw.write("function PrintThisDiv(id) {\n");
		fw.write("var HTMLContent = document.getElementById(id);\n");
		fw.write("var Popup = window.open('about:blank', id, 'width=500,height=500');\n");
		fw.write("Popup.document.writeln('<html><head>');\n");
		fw.write("Popup.document.writeln('<style TYP='text/css'>');\n");
		fw.write("Popup.document.writeln('body{font-family: verdana,arial,sans-serif;font-size:11;color:#333333;border-width: 1px;border-color: #a9c6c9;border-collapse: collapse;}');\n");
		fw.write("Popup.document.writeln('</style>');\n");
		fw.write("Popup.document.writeln('</head><body>');\n");
		fw.write("Popup.document.writeln('<a href='javascript:;' onclick='window.print();'>Imprimir</a>');\n");
		fw.write("Popup.document.writeln(HTMLContent.innerHTML);\n");
		fw.write("Popup.document.writeln('</body></html>');\n");
		fw.write("Popup.document.close();\n");
		fw.write("Popup.focus();\n");
		fw.write("}\n");
		fw.write("</script>\n");
		fw.write(    "<meta http-equiv='Content-TYP' content='text/html; charset=iso-8859-1'>\n");
		fw.write(      "<style>\n");
		fw.write(        ".imagenNO {display:none;}\n");
		fw.write(        ".imagen {display:table-cell;}\n");
		fw.write(      "</style>\n");
		fw.write(      "<script>\n");
		fw.write(      "function mostrarImagenes(recolocar)\n");
		fw.write(      "{\n");
		fw.write(        "for (x=1;x<document.images.length;x++)\n");
		fw.write(            "{\n");
		fw.write(                "document.images[x].className = 'imagen';\n");
		fw.write(            "}\n");
		fw.write(        "}\n");
		fw.write(      "function ocultarImagenes(recolocar)\n");
		fw.write(       "{\n");
		fw.write(        "for (x=1;x<document.images.length;x++)\n");
		fw.write(            "{\n");
		fw.write(                  "document.images[x].className = 'imagenNO';\n");
		fw.write(            "}\n");
		fw.write(        "}\n");
		fw.write(        "function MostrarTImagenes(recolocar)\n");
		fw.write(       "{\n");
		fw.write(        "for (x=3;x<document.images.length;x=x+3)\n");
		fw.write(            "{\n");
		fw.write(                  "document.images[x].height='700'\n");
		fw.write(                  "document.images[x].width='1000'\n");
		fw.write(            "}\n");
		fw.write(        "}\n");
		fw.write(        "function ImagenesP(recolocar)\n");
		fw.write(       "{\n");
		fw.write(        "for (x=3;x<document.images.length;x=x+3)\n");
		fw.write(            "{\n");
		fw.write(                  "document.images[x].height='211'\n");
		fw.write(                  "document.images[x].width='250'\n");
		fw.write(            "}\n");
		fw.write(        "}\n");
		fw.write(      "</script>\n");
		fw.write("<TABLE border='1' class='altrowstable'cellpadding='5' cellspacing='0'>\n");
		fw.write("<TR ALIGN=CENTER><TD COLSPAN=2>" + "<a href='https://www.bcp.com.pe/' title='bcp'>"+"<IMG SRC='"+bcpImg+"' style='width:100px;height:30px;'></a></TD><TD COLSPAN=2><a href='https://www.tcs.com/' title='tcs'><IMG SRC='"+tcsImg+"' style='width:100px;height:40px;'></a></TD></TR>\n");
		fw.write("<TR><TD><b>Escenario:</b> </TD><TD>"+scenario+"</TD><TD><b>Nombre de script:</b> </TD><TD>"+script_name+"</TD></TR>\n");
		fw.write("<TR><TD><b>Ejecutado por:</b> </TD><TD>"+System.getProperty("user.name")+"</TD><TD><b>Fecha Ejecucion:</b> </TD><TD>"+Utilities.dateTime(1)+"</TD></TR>\n");
		fw.write("<TR></TR>\n");
		fw.write("<title>Test Results</title>\n");
		fw.write("<head></head>\n");
		fw.write("<body>\n");
		fw.write("<font face='verdana'size='1'>\n");
		fw.write("<h1 align='center'>EXECUTION REPORT\n");
		fw.write("<table border='0' width='100%' height='50'>\n");
		fw.write("<TABLE border='1' class='altrowstable'cellpadding='5' cellspacing='0'>\n");
		fw.write("<tr>\n");
		fw.write("<td width='10%' bgcolor='#CCCCFF' align='center'><b><font color='#000000' face='verdana' size='2'>ID Caso</font></b></td>\n");
		fw.write("<td width='10%' bgcolor='#CCCCFF' align='Center'><b><font color='#000000' face='verdana' size='2'>NUMERO DE CUENTA</font></b></td>\n");
		fw.write("<td width='10%' bgcolor='#CCCCFF' align='Center'><b><font color='#000000' face='verdana' size='2'>ESTADO</font></b></td>\n");
		fw.write("<td width='10%' bgcolor='#CCCCFF' align='Center'><b><font color='#000000' face='verdana' size='2'>EVIDENCIA</font></b></td>\n");
		fw.write("</tr>\n");
				
	}
	
	public static void htmlFooter(FileWriter fw, int passCount, int failCount, String stTime, String enTime, int migData, int pBal, int iBal) throws Throwable {
		int total=passCount+failCount;
		fw.write("<TR ALIGN=center><td colspan='4' bgcolor='#e0e0d1'><b>RESUMEN EJECUCION</b></TD></TR>");
		fw.write("<TR ALIGN=center><TD colspan='1' bgcolor='#ccffcc'><b>PASO TOTAL</b></TD><TD colspan='1' bgcolor='#ccffcc'>"+passCount+"</TD><TD colspan='1' bgcolor='#ffd6cc'><b>FALLA TOTAL</b></TD><TD colspan='1' bgcolor='#ffd6cc'>"+failCount+"</TD></TR>");
		fw.write("<TR ALIGN=center><TD colspan='1'><b>TOTAL DE REGISTROS EN PLANTILLA</b></TD><TD colspan='1'>"+total+"</TD><TD colspan='1'><b>TOTAL REGISTROS MIGRADOS</b></TD><TD colspan='1'>"+migData+"</TD></TR>");
		fw.write("<TR ALIGN=center><TD colspan='1'><b>SALDO TOTAL EN PLANTILLA</b></TD><TD colspan='1'>"+pBal+"</TD><TD colspan='1'><b>SALDO TOTAL EN INTELLECT</b></TD><TD colspan='1'>"+iBal+"</TD></TR>");
		fw.write("<TR ALIGN=center><TD colspan='1'><b>HORA DE INICIO</b></TD><TD colspan='1'>"+stTime+"</TD><TD colspan='1'><b>HORA DE FINALIZACION</b></TD><TD colspan='1'>"+enTime+"</TD></TR>");
		fw.write("<TR></TR>");
		fw.write("</TABLE>");
		fw.write("<hr>");
		fw.write("<p>* POR FAVOR, INFORME INMEDIATAMENTE SI HAY ALGUNA DESVIACION EN EL (LOS) RESULTADO (S) / INFORME</p>");
		fw.write("</font>");
		fw.write("</body>");
		fw.write("</html>");
	}

	public static void htmlFooterSaldoTotal(FileWriter fw, int passCount, int failCount, String stTime, String enTime, int migData, float iBalPEN, float iBalUSD, float pBalPEN, float pBalUSD) throws Throwable {
		int total=passCount+failCount;
		fw.write("<TR ALIGN=center><td colspan='4' bgcolor='#e0e0d1'><b>RESUMEN EJECUCION</b></TD></TR>");
		fw.write("<TR ALIGN=center><TD colspan='1' bgcolor='#ccffcc'><b>PASO TOTAL</b></TD><TD colspan='1' bgcolor='#ccffcc'>"+passCount+"</TD><TD colspan='1' bgcolor='#ffd6cc'><b>FALLA TOTAL</b></TD><TD colspan='1' bgcolor='#ffd6cc'>"+failCount+"</TD></TR>");
		fw.write("<TR ALIGN=center><TD colspan='1'><b>TOTAL DE REGISTROS EN PLANTILLA</b></TD><TD colspan='1'>"+total+"</TD><TD colspan='1'><b>TOTAL REGISTROS MIGRADOS</b></TD><TD colspan='1'>"+migData+"</TD></TR>");
		fw.write("<TR ALIGN=center><TD colspan='1'><b>SALDO TOTAL EN PLANTILLA</b></TD><TD colspan='1'>"+"PEN: "+pBalPEN+", USD: "+pBalUSD+"</TD><TD colspan='1'><b>SALDO TOTAL EN INTELLECT</b></TD><TD colspan='1'>"+"PEN: "+iBalPEN+", USD: "+iBalUSD+"</TD></TR>");
		fw.write("<TR ALIGN=center><TD colspan='1'><b>HORA DE INICIO</b></TD><TD colspan='1'>"+stTime+"</TD><TD colspan='1'><b>HORA DE FINALIZACION</b></TD><TD colspan='1'>"+enTime+"</TD></TR>");
		fw.write("<TR></TR>");
		fw.write("</TABLE>");
		fw.write("<hr>");
		fw.write("<p>* POR FAVOR, INFORME INMEDIATAMENTE SI HAY ALGUNA DESVIACION EN EL (LOS) RESULTADO (S) / INFORME</p>");
		fw.write("</font>");
		fw.write("</body>");
		fw.write("</html>");
	}

	
	public static void addTestCase(FileWriter fw,int caso, String nroCust, String estado, String log , String screenshot) throws Throwable {
		fw.write("<td width='10%' bgcolor='#FFFFDC' align='center'><p><font color='#000000' face='Tahoma' size='2'>"+caso+"</font></p></td>");
		fw.write("<td width='10%' bgcolor='#FFFFDC' align='Center'><p><font color='#000000' face='Tahoma' size='2'>"+nroCust+"</font></p></td>");
		if(estado.equalsIgnoreCase("PASS")) {
			fw.write("<td width='10%' bgcolor='#FFFFDC' align='Center'><p><font color='Green' face='Tahoma' size='2'>"+estado+"</font></p></td>");
			fw.write("<td width='5%' bgcolor='#FFFFDC' valign='middle' align='center'><p><font color='Red' face='Tahoma' size='2'></a> <a href="+log+" style='color:Blue'>OK</a></font></p></td>");
		}else if(estado.equals("FAIL")) {
			fw.write("<td width='5%' bgcolor='#FFFFDC' valign='middle' align='center'><p><font color='Red' face='Tahoma' size='2'></a> <a href="+screenshot+" style='color:Red'>FAIL</a></font></p></td>");
			fw.write("<td width='5%' bgcolor='#FFFFDC' valign='middle' align='center'><p><font color='Red' face='Tahoma' size='2'></a> <a href="+log+" style='color:Blue'>Diferencia de datos</a></font></p></td>");
		}else {
			fw.write("<td width='5%' bgcolor='#FFFFDC' valign='middle' align='center'><p><font color='Red' face='Tahoma' size='2'></a> <a href="+screenshot+" style='color:Red'>FAIL</a></font></p></td>");
			if(estado.equals("Error")) {
				fw.write("<td width='10%' bgcolor='#FFFFDC' align='Center'><p><font color='#000000' face='Tahoma' size='2'>"+ "ERROR" +"</font></p></td>");
			}else {
				fw.write("<td width='10%' bgcolor='#FFFFDC' align='Center'><p><font color='#000000' face='Tahoma' size='2'>"+ "Registro no encontrado" +"</font></p></td>");
			}
		}
		fw.write("<p></p>");
		fw.write("</tr>");
	}
	
	public static String dateTime(int format) {
		String fechaTiempo = "";
		if(format==0) {
			fechaTiempo=  new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		}else if(format==1) {
			fechaTiempo=  new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());	
		}else {
	        System.setProperty("current.date.time", new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss").format(new Date()));
		}
		return fechaTiempo;
	}
	
	public static Iterator<Object[]> parseCSV(String fName) throws IOException{
		int i=0;
		BufferedReader input = null;
		File file = new File(fName);
		input = new BufferedReader(new FileReader(file));
		String line=null;
		ArrayList<Object[]> data = new ArrayList<Object[]>();
		while((line = input.readLine())!=null) {
			if(i>0) {
				String in = line.trim();
				String[] temp = in.split("#");
				List<Object> array = new ArrayList<Object>();
				for(String s : temp) {
	//				array.add(Integer.parseInt(s));
					array.add(s);
				}
				data.add(array.toArray());		
			}
			i++;
		}
		input.close();		
		
		return data.iterator();
	}
	
	public static int verifyField(Logger log,FileWriter f, String field, String csv, String app ) throws IOException {
		int val1 = 0;
		if(csv.trim().equals("")) {
			csv = "NULL";
		}
		if(app.trim().equals("")) {
			app = "NULL";
		}
		
		if(csv.trim().equalsIgnoreCase(app.trim())) {
			log.info(field+" : " + csv + " = " + app);	
			f.write(field+" |" + csv + "| = |" + app + "|\n");
		}else {
			log.error(field+" : " + csv + " = " + app);	
			f.write(field+" |" + csv + "| <> |" + app + "|\n");
			val1++;
		}
		return val1;
	}
	
	public static void writeLogHeader(Logger log,FileWriter f, int ite ) throws IOException {
		if(ite==1) {
			log.trace("Nombre de campo : Valor_Plantilla - Valor_Intellect");
		}
		f.write("Nombre de campo : Valor_Plantilla - Valor_Intellect\n");
		f.write("----------------------------------------------------\n");
	}
	
	public static void closeFile(FileWriter writeEFile, File eFil) throws IOException {
		writeEFile.close();
		if(eFil.exists()) {
			eFil.delete();
			}//delete the errFile
	}

	
	public static void logoutApp(WebDriver driver) throws InterruptedException, IOException {
		String parent = driver.getWindowHandle();
		Set<String> window = driver.getWindowHandles();
		Iterator<String> it = window.iterator();
		while(it.hasNext()) {
			String child = it.next();
			if(!parent.equals(child)) {
				driver.switchTo().window(child);
				if(driver.getTitle().contains("Intellect Suite")) {
					WebElement ele = driver.findElement(By.name("maintop"));
					driver.switchTo().frame(ele);
					driver.findElement(By.xpath("//*[@id=\"rootMenu\"]")).click();
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id=\"logout\"]")).click();
					Thread.sleep(2000);
					Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
					Thread.sleep(2000);
//					driver.quit();
				}
			}
		}
	}
	
//	public static void beforeClass(File fil, FileWriter writeFile, Properties prop, 
//			String temp, String desc) throws Throwable {
//		fil = new File(System.getProperty("user.dir")+"/Reports/Report_"+Utilities.dateTime(0)+".html");
//		writeFile = new FileWriter(fil);
//		Thread.sleep(1000);
//		prop = new Properties();
//		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
//		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//src/main/resources/chromedriver.exe");
//		ChromeOptions options = new ChromeOptions();
//        options.addArguments("--no-sandbox");
//        options.addArguments("--disable-dev-shm-usage");
//        driver = new ChromeDriver(options);
//		Utilities.reportHeader(writeFile, "Cliente_GFU", "ClienteConsultar");
//	}
		
}
