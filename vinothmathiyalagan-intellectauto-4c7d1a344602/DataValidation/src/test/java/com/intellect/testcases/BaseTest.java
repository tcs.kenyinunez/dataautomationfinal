package com.intellect.testcases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeClass;

import com.intellect.utilities.Utilities;

public class BaseTest {
	
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	
	
	@BeforeClass
	public WebDriver BaseMethod() throws IOException, InterruptedException {
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		log = LogManager.getLogger(BaseTest.class);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//src/main/resources/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("useAutomationExtension", false);
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        driver = new ChromeDriver(options);
		return driver;
	}
	
	public FileWriter wfile() throws IOException {
		writeFile = null;
		try {
			fil = new File(System.getProperty("user.dir")+"/Reports/Report_"+new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
			writeFile = new FileWriter(fil);
		}catch(IOException e) {
			e.getStackTrace();
		}
		return writeFile;
	}

}
