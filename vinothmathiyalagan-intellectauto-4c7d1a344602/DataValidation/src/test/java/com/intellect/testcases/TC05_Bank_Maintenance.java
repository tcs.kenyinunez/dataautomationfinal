package com.intellect.testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.Bank_Maintenance;
import com.intellect.pageobjects.MenuOption;
import com.intellect.utilities.Utilities;

public class TC05_Bank_Maintenance {
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	int iBal=0;
	int pBal=0;
	int migData=0;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	Bank_Maintenance cl;

	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC05_Bank_Maintenance.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_BankMaintenance_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Bank_Maintenance", "BankMaintenanceConsultar");
	}
	
	@BeforeMethod
	public WebDriver startTest() throws IOException, InterruptedException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuMaestro("Bank Maintenance");
			return driver;
		}
		return null;
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String codBanco,String codSucursal,String nomBanco,String nomSucursal,String codSWIFT,
			String dirSucursal1,String dirSucursal2,String pais,String codPostal,String personContacto,String numTelefPersonContacto,String transChipNo,
			String indBancoIntermediario) throws Throwable {

		try {
			log.warn("Iteration "+iteration + " begins **********************************"  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A1.11.5", "","Bank Maintenance", codBanco,"","");
			cl = new Bank_Maintenance(driver, log, writeEFile);
			if(indBancoIntermediario.equals("'")){indBancoIntermediario="";}
			int fieldCount = cl.Codigo_Banco(codBanco)+ cl.Codigo_Sucursal(codSucursal)+cl.Nombre_del_Banco(nomBanco)
					+cl.Nombre_Sucursal(nomSucursal)+cl.Codigo_SWIFT(codSWIFT)+cl.Direccion_Sucursal1(dirSucursal1)
					+cl.Direccion_Sucursal2(dirSucursal2)+cl.Pais(pais)+cl.Codigo_Postal(codPostal)+cl.Persona_Contacto(personContacto)
					+cl.Numero_Telefono_PersonaContacto(numTelefPersonContacto)+cl.Trans_Chip_No(transChipNo)+cl.Indicador_Banco_Intermediario(indBancoIntermediario);
			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, codBanco, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, codBanco, "FAIL", errFileName,Utilities.takeSnapShot(driver));	
			}
			migData++;
				
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")){
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, codBanco, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();	
			iteration++;
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/Bank_Maintenance.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {
		Utilities.htmlFooter(writeFile, passCount, failCount, stTime, Utilities.dateTime(1), migData, pBal, iBal);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}