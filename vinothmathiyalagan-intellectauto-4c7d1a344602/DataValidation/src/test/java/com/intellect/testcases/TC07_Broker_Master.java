package com.intellect.testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.Broker_Master;
import com.intellect.pageobjects.MenuOption;
import com.intellect.utilities.Utilities;

public class TC07_Broker_Master {
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	int iBal=0;
	int pBal=0;
	int migData=0;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	Broker_Master cl;

	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC07_Broker_Master.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_BrokerMaster_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Broker_Master", "BrokerMasterConsultar");
	}
	
	@BeforeMethod
	public WebDriver WebDriver() throws IOException, InterruptedException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuMaestro("Broker Contrapartes");
			return driver;
		}
		return null;
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String codContraparte,String nomContraparte,String nomCortoContraparte,String codContraparteExtranjero,String tipoContraparte,
			String direccion1,String direccion2,String ciudad,String departamento,String pais,String codPostal,String telefono,
			String fax,String nomPersonaContacto01,String nomPersonaContacto02,String nomSwift,String dirSwift,String numeroDocContraparte,
			String nomLocalContraparte,String dirLocalContraparte,String telfLocalContraparte,String emailContraparte,String nroFaxLocalContraparte,String codBIC,
			String tipoDoc) throws Throwable {

		try {
			log.warn("Iteration "+iteration + " begins **********************************"  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A1.2.5", "","Broker Contrapartes", codContraparte,"","");
			cl = new Broker_Master(driver, log, writeEFile);
			if(tipoDoc.equals("'")){tipoDoc="";}
			int fieldCount = cl.Codigo_Contraparte(codContraparte)+ cl.Nombre_Contraparte(nomContraparte)+cl.Nombre_corto_Contraparte(nomCortoContraparte)
					+cl.Codigo_Contraparte_Extrangero_Depositario_DTCC(codContraparteExtranjero)+cl.Tipo_Contraparte(tipoContraparte)+cl.Direccion1(direccion1)
					+cl.Direccion2(direccion2)+cl.Ciudad(ciudad)+cl.Departamento(departamento)+cl.Pais(pais)
					+cl.Codigo_Postal(codPostal)+cl.Telefono(telefono)+cl.Fax(fax)+cl.Nombre_Persona_Contacto01(nomPersonaContacto01)
					+cl.Nombre_Persona_Contacto02(nomPersonaContacto02)+cl.Nombre_Swift(nomSwift)+cl.Direccion_del_Swift(dirSwift)+cl.Nro_Documento_Contraparte(numeroDocContraparte)
					+cl.Nombre_Local_Contraparte(nomLocalContraparte)+cl.Direccion_Local_Contraparte(dirLocalContraparte)+cl.Nro_Telefono_Local_Contraparte(telfLocalContraparte)
					+cl.Correo_Electronico_Contraparte_Local(emailContraparte)+cl.Nro_Fax_Local_Contraparte(nroFaxLocalContraparte)+cl.Codigo_BIC(codBIC)+cl.Tipo_Documento(tipoDoc);
			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, codContraparte, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, codContraparte, "FAIL", errFileName,Utilities.takeSnapShot(driver));		
			}
			migData++;	
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")){
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, codContraparte, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();	
			iteration++;
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/MT_PARTY_GPU.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {
		Utilities.htmlFooter(writeFile, passCount, failCount, stTime, Utilities.dateTime(1), migData, pBal, iBal);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}
