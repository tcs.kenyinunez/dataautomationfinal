package com.intellect.testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.Withholding_Taxrate_Master;
import com.intellect.pageobjects.MenuOption;
import com.intellect.utilities.Utilities;

public class TC12_WithHolding_Tax_Rate_Master {
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	int iBal=0;
	int pBal=0;
	int migData=0;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	Withholding_Taxrate_Master cl;

	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC12_WithHolding_Tax_Rate_Master.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_WithholdingTaxrateMaster_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Withholding_Taxrate_Master", "WithholdingTaxrateMasterConsultar");
	}
	
	@BeforeMethod
	public WebDriver startTest() throws IOException, InterruptedException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuMaestro("WithHolding Tax Rate Master");
			return driver;
		}
		return null;
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String paisIngreso,String tipoEvento,String estatusResiClient,String tipoValor,String subtipoValor,
			String fecVigencia,String tasaImpositiva,String monMinimoImp,String monMaximoImp,String tipopersona) throws Throwable {

		try {
			log.warn("Iteration "+iteration + " begins **********************************"  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A1.15.5", "","WithHolding Tax Rate Master", paisIngreso, tipoEvento, estatusResiClient);
			cl = new Withholding_Taxrate_Master(driver, log, writeEFile);
			if(tipopersona.equals("'")){tipopersona="";}
			int fieldCount = cl.Pais_Ingreso(paisIngreso)+ cl.Tipo_Evento(tipoEvento)+cl.Estatus_Residencial_Cliente(estatusResiClient)
					+cl.Tipo_Valor(tipoValor)+cl.Subtipo_Valor(subtipoValor)+cl.Fecha_Vigencia(fecVigencia)
					+cl.Tasa_Impositiva(tasaImpositiva)+cl.Monto_Minimo_Impuestos(monMinimoImp)+cl.Monto_Maxmimo_Impuestos(monMaximoImp)
					+cl.Tipo_Persona(tipopersona);
			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, paisIngreso, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, paisIngreso, "FAIL", errFileName,Utilities.takeSnapShot(driver));		
			}
			migData++;			
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")){
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, paisIngreso, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();	
			iteration++;
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/WithHolding_Tax_Rate.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {
		Utilities.htmlFooter(writeFile, passCount, failCount, stTime, Utilities.dateTime(1), migData, pBal, iBal);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}
