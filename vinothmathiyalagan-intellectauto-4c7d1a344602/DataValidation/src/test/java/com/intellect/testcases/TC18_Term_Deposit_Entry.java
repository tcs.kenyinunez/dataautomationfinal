package com.intellect.testcases;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.MenuOption;
import com.intellect.pageobjects.Term_Deposit_Entry;
import com.intellect.utilities.Utilities;


public class TC18_Term_Deposit_Entry {
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	int value;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	float iBalPEN=0;
	float iBalUSD=0;
	float pBalPEN=0;
	float pBalUSD=0;
	int migData=0;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	Term_Deposit_Entry cl;

	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC18_Term_Deposit_Entry.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_TermDepositEntry_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Term_DepositEntry", "DepositoPlazo");
	}
	
	@BeforeMethod
	public WebDriver startTest() throws IOException, InterruptedException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuProcessOperations("Term Deposit Entry");
			return driver;
		}
		return null;
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String codCtaCustod,String indGenerCargo,String indGenerMsjSwift,String contraparte,
			String custGlobal,String codMoneda,String fechValorApertura,String numDias,
						 String fechVenc,String tasaInteres,String metodo,String compuesto,String frecInteres,
						 String coment,String codIsin,String impApertura,String intPagar,String impTotPagarVenc,
						 String fechOperacion) throws Throwable {

		try {
			log.warn("Iteration "+iteration + " begins **********************************"  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A2.18.1.5", "","Term Deposit Entry", codCtaCustod,"","");
			cl = new Term_Deposit_Entry(driver, log, writeEFile);
			if(fechOperacion.equals("'")){fechOperacion="";}
			int fieldCount = cl.Codigo_Cuenta_Custodia(codCtaCustod)+ cl.Indicador_Generacion_Cargo(indGenerCargo)
					+cl.Indicador_Generacion_Mensaje_SWIFT(indGenerMsjSwift)+cl.Contraparte(contraparte)+cl.Custodio_Global(custGlobal)
					+cl.Codigo_Moneda(codMoneda)+cl.Fecha_Valor_Apertura(fechValorApertura)+cl.Numero_Dias(numDias)
					+cl.Fecha_Vencimiento(fechVenc)+cl.Tasa_Interes(tasaInteres)+cl.Metodo(metodo)+cl.Compuesto(compuesto)
					+cl.Frecuencia_Interes(frecInteres)+cl.Comentarios(coment)+cl.Codigo_ISIN(codIsin)+cl.Importe_Apertura(impApertura)
					+cl.Interes_Pagar(intPagar) +cl.Importe_TotalPagar_Vencimiento(impTotPagarVenc)+cl.Fecha_Operacion(fechOperacion);

			System.out.println("Plantilla - "+ Float.parseFloat(impApertura) +" ; Intellect - "+cl.Importe_AperturaAPP());

			if(codMoneda.equals("PEN")){
				pBalPEN = pBalPEN + Float.parseFloat(impApertura);
				iBalPEN = iBalPEN + cl.Importe_AperturaAPP();
				
			}else if(codMoneda.equals("USD")){ 
				pBalUSD = pBalUSD + Float.parseFloat(impApertura);
				iBalUSD = iBalUSD + cl.Importe_AperturaAPP();
			} 

			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, codCtaCustod, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, codCtaCustod, "FAIL", errFileName,Utilities.takeSnapShot(driver));
			}
			migData++;
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")){
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, codCtaCustod, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();
			iteration++;
		}

	}

	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/DEPOSIT_UPLOAD.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {                    
		Utilities.htmlFooterSaldoTotal(writeFile, passCount, failCount, stTime, Utilities.dateTime(1), migData, iBalPEN, iBalUSD, pBalPEN, pBalUSD);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}