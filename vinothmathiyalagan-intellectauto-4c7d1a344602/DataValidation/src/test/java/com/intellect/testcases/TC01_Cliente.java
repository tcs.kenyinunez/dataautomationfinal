package com.intellect.testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.Client_GFU;
import com.intellect.pageobjects.MenuOption;
import com.intellect.utilities.Utilities;

public class TC01_Cliente {
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	int migData=0;
	int iBalPEN=0;
	int iBalUSD=0;
	int pBalPEN=0;
	int pBalUSD=0;
	Client_GFU cl;

	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC01_Cliente.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_ClienteGFU_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Cliente_GFU", "ClienteConsultar");
	}
	
	@BeforeMethod
	public WebDriver startTest() throws IOException, InterruptedException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuMaestro("Clientes");
			return driver;
		}
		return null;
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String codCuenta,String catCliente,String cuMaestra,String nombreCuenta,String nombreCliente,
			String fechaIng,String regPor,String fechaReg,String regAcceso,String infoAddDir,String valPor,String fechaValn,
			String dir1,String dir2,String telefono,String fax,String perContacto1,String perContacto2,String numeroDoc,
			String polBroker,String indCuMaestra,String pais,String estadoDept,String ciudad,String TipoDoc,String TipoPersona,
			String facturable,String paisDoc,String infPrefIdioma,String domicilio,String codMoneda,String factClienteIdioma,
			String CIC,String indCuentaMan,String fechaNac,String estatus,String codPostal,String fechaRegCliente,String indPropios) throws Throwable {

		try {
			log.warn("Iteration "+iteration + " begins ********************************** "+codCuenta  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A1.1.9", "","Clientes", codCuenta,"","");
			cl = new Client_GFU(driver, log, writeEFile);
			int fieldCount = cl.Codigo_Cuenta_Custodia(codCuenta)+ cl.Categoria_del_Cliente(catCliente)+cl.Cuenta_Maestra(cuMaestra)
					+cl.Nombre_de_Cuenta_Custodia(nombreCuenta)+cl.Nombre_del_Cliente(nombreCliente)+cl.Fecha_de_Ingreso(fechaIng)
					+cl.Registrado_por(regPor)+cl.Fecha_de_Registro(fechaReg)+cl.Registro_de_Accesos(regAcceso)+cl.Informacion_Adicional_de_Direccion(infoAddDir)
					+cl.Validado_por(valPor)+cl.Fecha_de_Validacion(fechaValn)+cl.Direccion1(dir1)+cl.Direccion2(dir2)+cl.Telefono(telefono)
					+cl.FAX(fax)+cl.Persona_de_Contacto1(perContacto1)+cl.Persona_de_Contacto2(perContacto2)+cl.Numero_de_Documento(numeroDoc)
					+cl.Poliza_Broker_Requerido(polBroker)+cl.Indicador_Cuenta_Maestra_Sub_Cuenta(indCuMaestra)+cl.Pais(pais)
					+cl.Estado_Departamento(estadoDept)+cl.Ciudad(ciudad)+cl.Tipo_de_Documento(TipoDoc)+cl.Tipo_de_Persona(TipoPersona)
					+cl.Facturable(facturable)+cl.Pais_del_Documento(paisDoc)+cl.De_informe_cliente_Preferencias_idioma(infPrefIdioma)
					+cl.Domicilio(domicilio)+cl.Codigo_Moneda(codMoneda)+cl.La_facturacion_cliente_Preferencias_Idioma(factClienteIdioma)
					+cl.Codigo_CIC(CIC)+cl.Indicador_Cuenta_Mancomunada_Individual(indCuentaMan)+cl.Fecha_Nacimiento(fechaNac)
					+cl.Codigo_Postal(codPostal)+cl.Fecha_Registro_Cliente(fechaRegCliente)+cl.Indicador_Propios_Terceros(indPropios)+cl.Estatus(estatus);
			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, codCuenta, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, codCuenta, "FAIL", errFileName,Utilities.takeSnapShot(driver));	
			}	
			migData++;
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")) {
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, codCuenta, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();	
			iteration++;
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/MT_CLIENT_GFU.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {
		Utilities.htmlFooterSaldoTotal(writeFile, passCount, failCount, stTime, Utilities.dateTime(1),migData,pBalPEN,pBalUSD,iBalPEN,iBalUSD);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}
