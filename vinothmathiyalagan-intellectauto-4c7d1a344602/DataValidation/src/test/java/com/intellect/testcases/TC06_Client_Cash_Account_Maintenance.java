package com.intellect.testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.Client_CashAccount_Maintenance;
import com.intellect.pageobjects.MenuOption;
import com.intellect.utilities.Utilities;

public class TC06_Client_Cash_Account_Maintenance {
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	int iBal=0;
	int pBal=0;
	int migData=0;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	Client_CashAccount_Maintenance cl;

	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC06_Client_Cash_Account_Maintenance.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_ClientCashAccountMaintenance_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Client_CashAccount_Maintenance", "ClientCashAccountMaintenanceConsultar");
	}
	
	@BeforeMethod
	public WebDriver startTest() throws IOException, InterruptedException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuMaestro("Client Cash Account Maintenance");
			return driver;
		}
		return null;
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String codCuentaCustodia,String codMoneda,String numCuentaDineraria,String modulo,String tipoCuentaDineraria,
			String codBanco) throws Throwable {

		try {
			log.warn("Iteration "+iteration + " begins **********************************"  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A1.12.5", "","Client Cash Account Maintenance", modulo,"","");
			cl = new Client_CashAccount_Maintenance(driver, log, writeEFile);
			if(codBanco.equals("'")){codBanco="";}
			int fieldCount = cl.Codigo_Cuenta_Custodia(codCuentaCustodia)+ cl.Codigo_Moneda(codMoneda)+cl.Numero_Cuenta_Dineraria(numCuentaDineraria)
					+cl.Modulo(modulo)+cl.Tipo_Cuenta_Dineraria(tipoCuentaDineraria)+cl.Codigo_Banco(codBanco);
			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, codCuentaCustodia, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, codCuentaCustodia, "FAIL", errFileName,Utilities.takeSnapShot(driver));	
			}
			migData++;		
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")){
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, codCuentaCustodia, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();	
			iteration++;
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/DL_CASHCLT_GFU.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {
		Utilities.htmlFooter(writeFile, passCount, failCount, stTime, Utilities.dateTime(1), migData, pBal, iBal);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}
