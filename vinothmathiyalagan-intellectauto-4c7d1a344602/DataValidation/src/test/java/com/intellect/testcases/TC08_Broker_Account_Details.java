package com.intellect.testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.Broker_Account_Details;
import com.intellect.pageobjects.MenuOption;
import com.intellect.utilities.Utilities;

public class TC08_Broker_Account_Details {
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	int iBal=0;
	int pBal=0;
	int migData=0;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	Broker_Account_Details cl;

	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC08_Broker_Account_Details.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_Broker_Account_Details_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Broker_Account_Details", "BrokerAccountDetailsConsultar");
	}
	
	@BeforeMethod
	public WebDriver startTest() throws IOException, InterruptedException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuMaestro("Broker Account Details");
			return driver;
		}
		return null;
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String codContra, String codBanco,String codSucursal, String numCuentaDineraria,
			String codMoneda, String codCuentaIBAN) throws Throwable {

		try {
			log.warn("Iteration "+iteration + " begins **********************************"  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A1.13.5", "","Broker Account Details", codContra,"","");
			cl = new Broker_Account_Details(driver, log, writeEFile);
			if(codCuentaIBAN.equals("'")){codCuentaIBAN="";}
			int fieldCount = cl.Codigo_Contraparte(codContra)+ cl.Codigo_Banco(codBanco)+ cl.Codigo_Sucursal(codSucursal)
				+cl.Numero_de_Cuenta_Dineraria(numCuentaDineraria)
				+cl.Codigo_Moneda(codMoneda)+cl.Codigo_Cuenta_IBAN(codCuentaIBAN);
			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, codContra, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, codContra, "FAIL", errFileName,Utilities.takeSnapShot(driver));	
			}
			migData++;
				
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")){
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, codContra, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();	
			iteration++;
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/Party_Account_Details.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {
		Utilities.htmlFooter(writeFile, passCount, failCount, stTime, Utilities.dateTime(1), migData, pBal, iBal);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}
