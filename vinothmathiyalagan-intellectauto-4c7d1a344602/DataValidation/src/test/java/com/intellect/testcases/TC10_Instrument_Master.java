package com.intellect.testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.Instrument_Master;
import com.intellect.pageobjects.MenuOption;
import com.intellect.utilities.Utilities;

public class TC10_Instrument_Master {
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	float iBalPEN=0;
	float iBalUSD=0;
	float pBalPEN=0;
	float pBalUSD=0;
	int migData=0;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	Instrument_Master cl;

	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC10_Instrument_Master.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_InstrumentMaster_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Instrument_Master", "InstrumentMasterConsultar");
	}
	
	@BeforeMethod
	public WebDriver startTest() throws IOException, InterruptedException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuMaestro("Valores");
			return driver;
		}
		return driver;
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String codValor,String codMonedaValor, String valNomActual,String subValor,String negociable,String cotizado,String mnemonico,String nomCortoValor,String codISINValor,
			String codValPrincipal,String fecInicioEmision,String fecVenEmision,String natuInteres,String tasaCuponInteres,String frecuenciaInteres,String tipoCalculoInteres,
			String fechaInteresAnterior,String sigFechaInteres,String loteComercializable,String codEmisor,String nomSwift,String colocadoPriv, String amorParcial,
			String fechaCambioLOA,String primeraFecInteres,String tasaDividendo,String desmaterializado,String notasObservaciones,String valNomInicial,String paisCotiBolsa,
			String tipoCnt,String extenDecimalValor,String tipoDividendo,String nomEmisor) throws Throwable {
		try {
			log.warn("Iteration "+iteration + " begins **********************************"  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A1.4.5", "","Valores", codValor,"","");
			cl = new Instrument_Master(driver, log, writeEFile);
			if(nomEmisor.equals("'")){nomEmisor="";}
			int fieldCount = cl.Codigo_Valor(codValor) 
					+cl.Codigo_moneda_valor(codMonedaValor)+cl.Valor_Nominal_Actual(valNomActual)
					+cl.Subtipo_del_Valor(subValor)+cl.Negociable(negociable)+cl.Cotizado(cotizado)
					+cl.Mnemonico(mnemonico)+cl.Nombre_corto_del_Valor(nomCortoValor)+cl.Codigo_ISIN_Valor(codISINValor)+cl.Codigo_Valor_Principal(codValPrincipal)
					+cl.Fecha_Inicio_Emision(fecInicioEmision)+cl.Fecha_Vencimiento_Emision(fecVenEmision)+cl.Naturaleza_Interes(natuInteres)+cl.Tasa_Cupon_Interes(tasaCuponInteres)
					+cl.Frecuencia_interes(frecuenciaInteres)+cl.Tipo_Calculo_Interes(tipoCalculoInteres)+cl.Fecha_Interes_Anterior(fechaInteresAnterior)
					+cl.Siguiente_Fecha_Interes(sigFechaInteres)+cl.Lote_Comercializable(loteComercializable)+cl.Codigo_Emisor(codEmisor)+cl.Nombre_SWIFT(nomSwift)
					+cl.Colocado_Privadamente(colocadoPriv)+cl.Amortizacion_Parcial(amorParcial)+cl.Fecha_cambio_LOA(fechaCambioLOA)+cl.Primera_Fecha_Interes(primeraFecInteres)
					+cl.Tasa_Dividendo(tasaDividendo)+cl.Desmaterializado(desmaterializado)+cl.Notas_Observaciones(notasObservaciones)+cl.Valor_Nominal_Inicial(valNomInicial)
					+cl.Pais_Cotizacion_Bolsa(paisCotiBolsa)+cl.Tipo_Cantidad(tipoCnt)+cl.Extension_Decimal_para_Valor(extenDecimalValor)+cl.Tipo_Dividendo(tipoDividendo)
					+cl.Nombre_del_Emisor(nomEmisor);
			
			System.out.println("Plantilla - "+Float.parseFloat(valNomActual)+" ; Intellect - "+cl.Valor_Nominal_ActualAPP());
			
			if(codMonedaValor.equals("PEN")){
				pBalPEN=pBalPEN+Float.parseFloat(valNomActual);
				iBalPEN=iBalPEN+cl.Valor_Nominal_ActualAPP();
			}else if(codMonedaValor.equals("USD")){
				pBalUSD=pBalUSD+Float.parseFloat(valNomActual);
				iBalUSD=iBalUSD+cl.Valor_Nominal_ActualAPP();
			}

			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, codValor, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, codValor, "FAIL", errFileName,Utilities.takeSnapShot(driver));		
			}
			migData++;
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")){
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, codValor, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();	
			iteration++;
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/MT_INSTRUMENT_GFU.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {
		Utilities.htmlFooterSaldoTotal(writeFile, passCount, failCount, stTime, Utilities.dateTime(1), migData, iBalPEN, iBalUSD, pBalPEN, pBalUSD);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}
