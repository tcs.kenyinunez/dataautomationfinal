package com.intellect.testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.intellect.pageobjects.AppLogin;
import com.intellect.pageobjects.Client_GFU;
import com.intellect.pageobjects.Depositorio;
import com.intellect.pageobjects.MenuOption;
import com.intellect.utilities.Utilities;

public class TC02_Client_Depository_Mapping{
	
	WebDriver driver;
	File fil;
	File eFil;
	FileWriter writeFile;
	FileWriter writeEFile;
	Logger log;
	Properties prop;
	String errFilePath;
	boolean loginFlag =true;
	String stTime = Utilities.dateTime(1);
	int passCount=0;
	int failCount=0;
	int iteration=1;
	int migData=0;
	int iBalPEN=0;
	int iBalUSD=0;
	int pBalPEN=0;
	int pBalUSD=0;
	String status;
	String errFileName;
	MenuOption menu;
	AppLogin signIn;
	Client_GFU cl;
	Depositorio depo;
	
	@BeforeClass
	public void initializeTest() throws Throwable {
		log = LogManager.getLogger(TC02_Client_Depository_Mapping.class);
		fil = new File(System.getProperty("user.dir")+"/Reports/Report_Client-Depository_"+Utilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		Utilities.reportHeader(writeFile, "Cliente_Depo_Mapping", "ClienteConsultar");
	}
	
	@BeforeMethod
	public void startTest() throws InterruptedException, IOException, AWTException {
		errFileName = System.getProperty("user.dir")+"/Reports/Report/Docs/file_"+Utilities.dateTime(0)+".txt";
		eFil = new File(errFileName);
		writeEFile = new FileWriter(eFil);		
		if(loginFlag) {
			signIn = new AppLogin(driver);
			driver = signIn.login(prop.getProperty("url"),prop.getProperty("user"), prop.getProperty("password"));
			loginFlag=false;
			menu = new MenuOption(driver);
			menu.menuMaestro("Clientes");
		}
	}
	
	
	@Test(dataProvider="testData")
	public void testCase(String codCC,String codDepo,String codCliDepo, String idDepo) throws Throwable {

		try {
			log.warn("Iteration "+iteration + " begins ********************************** "+codCC  );
			Utilities.writeLogHeader(log, writeEFile, iteration);
			driver = menu.consultar("A1.1.9", "","Clientes", codCC,"","");	
			depo = new Depositorio(driver, failCount, log, writeEFile);
			int fieldCount = depo.Codigo_Cuenta_Custodia(codCC)+depo.DepoValidation(codDepo, codCliDepo, idDepo);
			writeEFile.close();
			if(fieldCount==0) {
				passCount++;Utilities.addTestCase(writeFile, iteration, codCC, "PASS", errFileName,"NA");
			}else {
				failCount++;Utilities.addTestCase(writeFile, iteration, codCC, "FAIL", errFileName,Utilities.takeSnapShot(driver));	
			}	
			migData++;
		}catch(Exception e) {
			failCount++;
			System.out.println("Element not found Exception. "+e.getMessage());//No se encontraron registros para Consultar - for No Records
			if(driver.getPageSource().contains("No se encontraron registros")) {
				log.error("No se encontraron registros para Consultar");status="No Registro";
			}else {status="ERROR";}
			Utilities.closeFile(writeEFile, eFil);
			Utilities.addTestCase(writeFile, iteration, codCC, status, errFileName,Utilities.takeSnapShot(driver));
			Thread.sleep(1000);
		}finally {
			driver.switchTo().defaultContent();	
			iteration++;
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return Utilities.parseCSV(System.getProperty("user.dir")+"/InputData/MT_CLI_DEPO_MAP_GFU.csv");
	}
	

	@AfterClass
	public void tearDown() throws Throwable {
		Utilities.htmlFooterSaldoTotal(writeFile, passCount, failCount, stTime, Utilities.dateTime(1),migData,pBalPEN,pBalUSD,iBalPEN,iBalUSD);
		writeFile.close();
		Utilities.logoutApp(driver);
	}
	
}
