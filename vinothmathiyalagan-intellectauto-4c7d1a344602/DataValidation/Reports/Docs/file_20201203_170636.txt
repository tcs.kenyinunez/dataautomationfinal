Nombre de campo : Valor_Plantilla - Valor_Intellect
----------------------------------------------------
Codigo Cuenta Custodia |10005| = |10005|
Indicador Generacion de Cargo/Abono |N| = |N|
Indicador Generacion del Mensaje SWIFT |N| = |N|
Contraparte |142| <> |0142|
Custodio Global |BBH| = |BBH|
Codigo Moneda |USD| = |USD|
Fecha Valor/Fecha de Apertura |01/12/2020| = |01/12/2020|
Numero de Dias |5| = |5|
Fecha de Vencimiento |06/12/2020| = |06/12/2020|
Tasa de Interes |6| = |6|
Metodo |B| = |B|
Compuesto |Y| = |Y|
Frecuencia de Interes |O| = |O|
Comentarios |TEST| = |TEST|
Codigo ISIN |NULL| = |NULL|
Importe de Apertura |2| = |2|
Interes a Pagar |4| <> |2|
Importe Total a Pagar en el Vencimiento id |1| <> |4|
Fecha de Operacion |01/12/2020| = |01/12/2020|
