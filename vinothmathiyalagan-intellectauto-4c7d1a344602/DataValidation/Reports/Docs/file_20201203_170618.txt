Nombre de campo : Valor_Plantilla - Valor_Intellect
----------------------------------------------------
Codigo Cuenta Custodia |100017| = |100017|
Indicador Generacion de Cargo/Abono |Y| <> |N|
Indicador Generacion del Mensaje SWIFT |N| = |N|
Contraparte |1| <> |0071|
Custodio Global |NULL| <> |BBH|
Codigo Moneda |USD| = |USD|
Fecha Valor/Fecha de Apertura |23/03/2018| <> |01/12/2020|
Numero de Dias |15| <> |23|
Fecha de Vencimiento |23/04/2018| <> |24/12/2020|
Tasa de Interes |5| = |5|
Metodo |NULL| <> |B|
Compuesto |Y| <> |N|
Frecuencia de Interes |O| = |O|
Comentarios |NULL| <> |TEST|
Codigo ISIN |NULL| = |NULL|
Importe de Apertura |5.3| <> |2|
Interes a Pagar |6| <> |2|
Importe Total a Pagar en el Vencimiento id |2| <> |4|
Fecha de Operacion |23/03/2018| <> |01/12/2020|
